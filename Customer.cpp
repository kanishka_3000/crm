#include "Customer.h"
#include <CRMDBFields.h>
Customer::Customer()
{
    s_PersistatName = "Customer";
    s_DefName = ENTITY_CUSTOMER;
    s_Key = FLD_PHONE;
}

QString Customer::GetCustomerName()
{
    return map_Data.value(FDNM_CRM_CUSTOMER_NAME).toString();
}

int Customer::GetContactNo()
{
    return map_Data.value(FDNM_CRM_CUSTOMER_PHONE).toInt();
}

QString Customer::GetAddress()
{
    return GetData(FDNM_CRM_CUSTOMER_ADDRESS).toString();
}

