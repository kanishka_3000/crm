/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "OrderFilterCtrl.h"
#include <CRMDBFields.h>
#include <GenericWnd.h>
OrderFilterCtrl::OrderFilterCtrl(QWidget *parent) :
GenericFilterCtrl(parent)
{
    setupUi(this);
    txt_CustomerID->setProperty(FIELD, FDNM_CRM_CLIENTORDER_CUSTOMERID);
    dt_DeliveryFrom->setProperty(FIELD, FDNM_CRM_CLIENTORDER_DELIVERYDATE);
    cmb_OrderStatus->setProperty(FIELD, FDNM_CRM_CLIENTORDER_STATE);
    cmb_OrderStatus->setProperty(ENUM_NAME, E_ORDER_STATE);
    SetAsFilterBuddy(dt_DeliveryFrom, dt_DelivertyTo);
    AddCheckBuddies(dt_DeliveryFrom,chk_DeliveryDate);
    SetCurrentDateToAllFields();
    SetClearButton(btn_Clear);
}

OrderFilterCtrl::~OrderFilterCtrl()
{
    delete p_Clm;
}

void OrderFilterCtrl::OnCreateCtrl()
{
    connect(btn_Apply,SIGNAL(clicked(bool)),this, SLOT(OnApply()));
    cmb_OrderStatus->addItem("");
    FillComboWithEnum(cmb_OrderStatus, E_ORDER_STATE);

     p_Clm = new FieldCompleter(p_Parent->GetApplication(),TBLNM_CRM_CUSTOMER);
     p_Clm->SetSource(txt_CustomerName, FDNM_CRM_CUSTOMER_NAME);
     p_Clm->SetDestination(txt_CustomerID, FDNM_CRM_CUSTOMER_PHONE);
     chk_DeliveryDate->setMinimumHeight(lbl_CustName->geometry().height() + 4);
}

void OrderFilterCtrl::SetClearButton(QPushButton *pClearButton)
{
    connect(pClearButton,SIGNAL(clicked(bool)),this,SLOT(ClearFields()));
}

void OrderFilterCtrl::OnApply()
{
    OnFilter();
}
