/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "BasicServiceSelector.h"

#include <crmglobaldefs.h>
#include <CRMDBFields.h>
#include <QMessageBox>
#include <QDebug>
BasicServiceSelector::BasicServiceSelector(QWidget *parent) :
GenericCtrl(parent),b_MultipleItems(false)
{
    setupUi(this);
}

BasicServiceSelector::~BasicServiceSelector()
{
}

void BasicServiceSelector::OnCreateCtrl()
{
    SetComboCompleter(cmb_ServiceName);
    SetComboCompleter(cmb_ServiceType);
    cmb_ServiceType->addItem("");
    QList<std::shared_ptr<Entity>> lstServiceGroups = Entity::FindAllInstances(ENTITY_SERVICE_GROUP);
    foreach(std::shared_ptr<Entity> pEntity, lstServiceGroups)
    {
        std::shared_ptr<ServiceGroup> pServiceGroup = std::static_pointer_cast<ServiceGroup>(pEntity);
        cmb_ServiceType->addItem(pServiceGroup->GetServiceGroupName(),pServiceGroup->GetServiceGroupID());
    }

    connect(cmb_ServiceType,SIGNAL(activated(QString)),this,SLOT(OnServiceTypeSelected(QString)));
    connect(cmb_ServiceName,SIGNAL(activated(QString)),this, SLOT(OnServiceSelected(QString)));
}

void BasicServiceSelector::OnServiceTypeSelected(QString sServiceGroup)
{
    (void)sServiceGroup;
    cmb_ServiceName->clear();
    map_CurrentItems.clear();
    cmb_ServiceName->addItem("");

    QString sData = cmb_ServiceType->currentData().toString();
    QList<std::shared_ptr<Entity>> lstServices = Entity::FindInstances(FDNM_CRM_SERVICES_GROUPID,sData,ENTITY_SERVICE);
    b_MultipleItems = false;
    foreach(std::shared_ptr<Entity> pEntity, lstServices)
    {
        std::shared_ptr<Service> pService = std::static_pointer_cast<Service>(pEntity);
        int iStatus = pService->GetStatus();
        if(iStatus == 1)
        {
            p_Logger->WriteLog("Ignoring Service since inactive %s", qPrintable(pService->GetServiceName()));
            continue;
        }
        cmb_ServiceName->addItem(pService->GetServiceName(),pService->GetServiceID());
        if(map_CurrentItems.contains(pService->GetServiceName()))
        {
            p_Logger->WriteLog("%s is already in the current items map", qPrintable(pService->GetServiceName()));
            b_MultipleItems = true;
        }
        map_CurrentItems.insert(pService->GetServiceName(), pService);
    }

}

void BasicServiceSelector::OnServiceSelected(QString sService)
{
    if(sService == "")
    {
        return;
    }
    if(b_MultipleItems)
    {
        QMessageBox::warning(this,tr("Mutiple Services"), tr("Multiple services with same name detected. Only last one added"));

    }
    std::shared_ptr<Service> pSelectedService = map_CurrentItems.value(sService);
    QList<std::shared_ptr<Entity>> oOutput;
    oOutput.push_back(pSelectedService);
    NotifiyDataSelected(oOutput,false);
}
