/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ProductionGraphFilterCtrl.h"
#include <Service.h>
#include <CRMDBFields.h>
#include <QMessageBox>
ProductionGraphFilterCtrl::ProductionGraphFilterCtrl(QWidget *parent) :
    GenericFilterCtrl(parent)
{
    setupUi(this);
    SetFieldProperty(dt_StartDate, FILTER_PRODCTION_START_DATE);
    SetFieldProperty(dt_EndDate, FILTER_PRODUCTION_END_DATE);
    SetFieldProperty(cmb_Product, FILTER_PRODUCT);
    SetFieldProperty(cmb_Type, FILTER_GRAPH);
    SetComboCompleter(cmb_Product);
}


void ProductionGraphFilterCtrl::FillServices()
{
    QList<std::shared_ptr<Entity>> lstServices = Entity::FindAllInstances(TBLNM_CRM_SERVICES);
    foreach(std::shared_ptr<Entity> pEntity, lstServices)
    {
        std::shared_ptr<Service> pService = std::static_pointer_cast<Service>(pEntity);
        AddComboDataWithTooltip(cmb_Product, pService->GetServiceName());
    }
    cmb_Product->addItem("");
}

void ProductionGraphFilterCtrl::OnCreateCtrl()
{
    SetCurrentDateToAllFields();
    FillServices();
    AddComboDataWithTooltip(cmb_Type, PRODUCT_COUNT_GRAPH);
    AddComboDataWithTooltip(cmb_Type, PRODUCTS_PER_TIME);
    AddComboDataWithTooltip(cmb_Type, PAYMENT_GRAPH);

    connect(btn_Apply, SIGNAL(clicked(bool)),this, SLOT(OnFilter()));

}


void ProductionGraphFilterCtrl::OnFilter()
{
    if(dt_EndDate->date() < dt_StartDate->date())
    {
        QMessageBox::warning(this, "Dates not matching", "End date higher than start date");
        return;
    }
    GenericFilterCtrl::OnFilter();

}
