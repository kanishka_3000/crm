/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef CRMMAINWND_H
#define CRMMAINWND_H

#include <QMainWindow>
#include <MainWnd.h>
class CRMMainWnd : public MainWnd
{
    Q_OBJECT

public:
    CRMMainWnd(QWidget *parent = 0);
    ~CRMMainWnd();
    virtual void Initilize(Application* pApplication);
public slots:
    virtual void OnMenuManageCustomers();
    virtual void OnMenuManageServiceGroups();
    virtual void OnMenuManageServices();
    virtual void OnMenuOrders();
    virtual void OnMenuNewOrder();
    virtual void OnMenuNewCustomer();
    virtual void OnMenuProduction();
    virtual void OnPaymentQuery();
};

#endif // CRMMAINWND_H
