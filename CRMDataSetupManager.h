/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef CRMDATASETUPMANAGER_H
#define CRMDATASETUPMANAGER_H
#include <DataSetupManager.h>

class CRMDataSetupManager : public DataSetupManager
{
public:
    CRMDataSetupManager();

    // DataSetupManager interface
public:
    virtual void Setup(Connection *pConnection, Logger* pLogger);
};

#endif // CRMDATASETUPMANAGER_H
