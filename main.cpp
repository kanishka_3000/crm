#include "CRMMainWnd.h"
#include <QApplication>
#include <Application.h>
#include <CRMObjecFactory.h>
#include <CRMApplication.h>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CRMObjecFactory oFactory;
    CRMApplication oApp;
    oApp.Initialize(&a,&oFactory);

    return a.exec();
}
