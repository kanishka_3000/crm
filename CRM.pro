#-------------------------------------------------
#
# Project created by QtCreator 2016-01-23T13:32:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

INCLUDEPATH += "../ApplicationBase"
DEPENDPATH += "../ApplicationBase"
#RC_FILE = crm.rc
include(../ApplicationBase/ApplicationBase.pri)

TRANSLATIONS = languages/sinhala.ts
TARGET = CRM
TEMPLATE = app

SOURCES += $$PWD/CRMMainWnd.cpp \
    $$PWD/CRMObjecFactory.cpp \
    $$PWD/CRMApplication.cpp \
    $$PWD/Customer.cpp \
    $$PWD/CRMEntityFactory.cpp \
    $$PWD/CustomerRegisterCtrl.cpp \
    $$PWD/CustomerRegisterWnd.cpp \
    $$PWD/ManageCustomerWnd.cpp \
    $$PWD/ManageCustomerFilterCtrl.cpp \
    $$PWD/ServiceGroup.cpp \
    $$PWD/AddServiceGroupCtrl.cpp \
    $$PWD/AddServiceGroupWnd.cpp \
    $$PWD/ManageServicesGroupCtrl.cpp \
    $$PWD/ManageServicesGroupWnd.cpp \
    $$PWD/Service.cpp \
    $$PWD/AddServicesCtrl.cpp \
    $$PWD/AddServicesWnd.cpp \
    $$PWD/ServiceSelector.cpp \
    $$PWD/Order.cpp \
    $$PWD/Task.cpp \
    $$PWD/OrderCtrl.cpp \
    $$PWD/OrderWnd.cpp \
    $$PWD/ServiceDataHandler.cpp \
    $$PWD/OrderQueryWnd.cpp \
    $$PWD/OrderFilterCtrl.cpp \
    $$PWD/Payment.cpp \
    $$PWD/PaymentEntryCtrl.cpp \
    $$PWD/PaymentEntryWnd.cpp \
    $$PWD/OrderNotificationResolver.cpp \
    $$PWD/BasicServiceSelector.cpp \
    $$PWD/CRMDataSetupManager.cpp \
    $$PWD/ProductionGraphWnd.cpp \
    $$PWD/ProductionGraphFilterCtrl.cpp \
    $$PWD/entity/ProductCountDataCollector.cpp \
    $$PWD/entity/ProductPerTimeDataCollector.cpp \
    $$PWD/PaymentQueryWnd.cpp \
    $$PWD/PaymentQueryFilterCtrl.cpp \
    $$PWD/entity/PaymentPerTimeDataCollector.cpp \
    $$PWD/CRMPreferenceWnd.cpp \
    $$PWD/CMSPreferenceTab.cpp \
    $$PWD/main.cpp

HEADERS  += $$PWD/CRMMainWnd.h \
    $$PWD/CRMObjecFactory.h \
    $$PWD/CRMApplication.h \
    $$PWD/Customer.h \
    $$PWD/crmglobaldefs.h \
    $$PWD/CRMEntityFactory.h \
    $$PWD/CustomerRegisterCtrl.h \
    $$PWD/CustomerRegisterWnd.h \
    $$PWD/ManageCustomerWnd.h \
    $$PWD/ManageCustomerFilterCtrl.h \
    $$PWD/ServiceGroup.h \
    $$PWD/AddServiceGroupCtrl.h \
    $$PWD/AddServiceGroupWnd.h \
    $$PWD/ManageServicesGroupCtrl.h \
    $$PWD/ManageServicesGroupWnd.h \
    $$PWD/Service.h \
    $$PWD/AddServicesCtrl.h \
    $$PWD/AddServicesWnd.h \
    $$PWD/ServiceSelector.h \
    $$PWD/CRMDBFields.h \
    $$PWD/Order.h \
    $$PWD/Task.h \
    $$PWD/OrderCtrl.h \
    $$PWD/OrderWnd.h \
    $$PWD/ServiceDataHandler.h \
    $$PWD/OrderQueryWnd.h \
    $$PWD/OrderFilterCtrl.h \
    $$PWD/Payment.h \
    $$PWD/PaymentEntryCtrl.h \
    $$PWD/PaymentEntryWnd.h \
    $$PWD/OrderNotificationResolver.h \
    $$PWD/BasicServiceSelector.h \
    $$PWD/vRMDataSetupManager.h \
    $$PWD/ProductionGraphWnd.h \
    $$PWD/ProductionGraphFilterCtrl.h \
    $$PWD/entity/ProductCountDataCollector.h \
    $$PWD/entity/ProductPerTimeDataCollector.h \
    $$PWD/PaymentQueryWnd.h \
    $$PWD/PaymentQueryFilterCtrl.h \
    $$PWD/entity/PaymentPerTimeDataCollector.h \
    $$PWD/CRMPreferenceWnd.h \
    $$PWD/CMSPreferenceTab.h

FORMS    += \
  $$PWD/MainWnd.ui \
    $$PWD/CustomerRegisterCtrl.ui \
    $$PWD/CustomerRegisterWnd.ui \
    $$PWD/ManageCustomerWnd.ui \
    $$PWD/ManageCustomerFilterCtrl.ui \
    $$PWD/AddServiceGroupCtrl.ui \
    $$PWD/AddServiceGroupWnd.ui \
    $$PWD/ManageServicesGroupCtrl.ui \
    $$PWD/ManageServicesGroupWnd.ui \
    $$PWD/AddServicesCtrl.ui \
    $$PWD/AddServicesWnd.ui \
    $$PWD/OrderCtrl.ui \
    $$PWD/OrderWnd.ui \
    $$PWD/OrderQueryWnd.ui \
    $$PWD/OrderFilterCtrl.ui \
    $$PWD/PaymentEntryCtrl.ui \
    $$PWD/PaymentEntryWnd.ui \
    $$PWD/BasicServiceSelector.ui \
    $$PWD/ProductionGraphWnd.ui \
    $$PWD/ProductionGraphFilterCtrl.ui \
    $$PWD/PaymentQueryWnd.ui \
    $$PWD/PaymentQueryFilterCtrl.ui \
    $$PWD/CMSPreferenceTab.ui

RESOURCES += \
   $$PWD/CRM.qrc
