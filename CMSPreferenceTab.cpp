/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "CMSPreferenceTab.h"
#include <crmglobaldefs.h>
CMSPreferenceTab::CMSPreferenceTab(QWidget *parent) :
    BasePreferenceTab(parent)
{
    setupUi(this);
    SetPrefName(txt_Root, PREF_ROOT);
    SetPrefName(txt_Source, PREF_SOURCE);
    SetPrefName(txt_Processing, PREF_PROCESS);
    SetPrefName(txt_Complete, PREF_COMPLTE);
    SetPrefName(chk_Enable, PREF_CNT_ENABLE);

}

void CMSPreferenceTab::OnOpenRoot()
{
    QString sRootLocation = QFileDialog::getExistingDirectory(this,tr("Open Root location"), QDir::homePath());
    txt_Root->setText(sRootLocation);
}


void CMSPreferenceTab::OnCreateCtrl()
{
    connect(btn_Root, SIGNAL(clicked(bool)),this, SLOT(OnOpenRoot()));
}
