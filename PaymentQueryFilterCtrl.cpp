/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "PaymentQueryFilterCtrl.h"
#include <CRMDBFields.h>
PaymentQueryFilterCtrl::PaymentQueryFilterCtrl(QWidget *parent) :
    GenericFilterCtrl(parent)
{
    setupUi(this);
    SetFieldProperty(dt_From, FDNM_CRM_PAYMENT_DATE);
    SetFieldProperty(txt_OrderID, FDNM_CRM_PAYMENT_ORDERID);
    SetAsFilterBuddy(dt_From, dt_To);
    SetCurrentDateToAllFields();
}


void PaymentQueryFilterCtrl::OnCreateCtrl()
{
    connect(btn_Apply, SIGNAL(clicked(bool)),this, SLOT(OnFilter()));
}
