/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "OrderWnd.h"


OrderWnd::OrderWnd(MainWnd *pMainWnd, QString sTitle) :
   GenericWnd(pMainWnd, sTitle)
{
    setupUi(this);

}

OrderWnd::~OrderWnd()
{

}

void OrderWnd::InitializeWithCustomer(std::shared_ptr<Customer> pCustomer)
{
    QString sTitle = QString(tr("Manage Order For %1")).arg(pCustomer->GetCustomerName());
    setWindowTitle(sTitle);
    p_Ctrl->InitializeWithCustomer(pCustomer);
}

void OrderWnd::InitliazeWithOrder(std::shared_ptr<Order> pOrder)
{
    p_Ctrl->EditOrder(pOrder);
}

void OrderWnd::OpenIndependant()
{
    p_Ctrl->OpenIndependant();
}

void OrderWnd::OnCreate()
{
    ReSize(457,501);
}
