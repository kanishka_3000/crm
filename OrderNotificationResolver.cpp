/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "OrderNotificationResolver.h"
#include <Order.h>
#include <CRMDBFields.h>
#include <Application.h>
#include <OrderQueryWnd.h>
OrderNotificationResolver::OrderNotificationResolver()
{

}

void OrderNotificationResolver::GetData(int iYear, int iMonth, QList<QDate>& lstDates, QList<QString>& lstNotifications)
{
    QString sStart = QString::asprintf("%d-%02d-01",iYear,iMonth);
    QString sEnd = QString::asprintf("%d-%02d-31",iYear,iMonth);

    QList<std::shared_ptr<Entity>> lstOrders;
    lstOrders = Entity::FindInstances(FDNM_CRM_CLIENTORDER_DELIVERYDATE,sStart, sEnd,TBLNM_CRM_CLIENTORDER);
    foreach(std::shared_ptr<Entity> pEOrder, lstOrders)
    {
        std::shared_ptr<Order> pOrder = std::static_pointer_cast<Order>(pEOrder);
        QString sDeliveryDate = pOrder->GetDeliveryDate();
        QDate oDate = QDate::fromString(sDeliveryDate, "yyyy-MM-dd");
        lstDates.push_back(oDate);
        QString sCutomerName = "INVALID";
        std::shared_ptr<Customer> pCustomer = pOrder->GetCustomer();
        if(pCustomer)
        {
            sCutomerName = pCustomer->GetCustomerName();
        }
        QString sNotification = QString("Order ID %1 should be delivered on %2 to %3").arg(pOrder->GetOrderID()).arg(pOrder->GetDeliveryDate()).arg(sCutomerName);
        lstNotifications.push_back(sNotification);
    }
}

void OrderNotificationResolver::OnDateSelected(QDate &oDate)
{
    QString sDate = oDate.toString(DATE_FORMAT);
    QMap<QString,QString> mapFilter;
    mapFilter.insert(FDNM_CRM_CLIENTORDER_DELIVERYDATE,sDate);
    OrderQueryWnd* pWnd = static_cast<OrderQueryWnd*>(Application::GetApplication()->CreateWnd(WND_ORDER_QUERy));
    pWnd->SetFilter(mapFilter);
}

