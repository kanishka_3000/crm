/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef CRMAPPLICATION_H
#define CRMAPPLICATION_H
#include <QObject>
#include <Application.h>
class CRMApplication: public Application
{
    Q_OBJECT
public:
    CRMApplication(QObject *parent = 0);
protected:
    virtual void RegisterWindows();

   virtual void CustomizeDefaultWindows(GenericWnd* pWnd, int iType);

    // Application interface
public:
    virtual QString GetApplicationName() override;
};

#endif // CRMAPPLICATION_H
