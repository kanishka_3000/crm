/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGESERVICESGROUPWND_H
#define MANAGESERVICESGROUPWND_H

#include <QWidget>
#include "ui_ManageServicesGroupWnd.h"
#include <GenericWnd.h>

class ManageServicesGroupWnd : public GenericWnd, public Ui::ManageServicesGroupWnd
{
    Q_OBJECT

public:
    explicit ManageServicesGroupWnd(MainWnd* pParent, QString sTitle);
    virtual void OnCreate();
    virtual void OnChildRemoveRequest(GenericCtrl* pChild);
    ~ManageServicesGroupWnd();

public slots:
    void OnAddService();
    virtual void OnEditRequest(QString sKeyValue);
protected:

private:
    AddServiceGroupCtrl* p_EditCtrl;
};

#endif // MANAGESERVICESGROUPWND_H
