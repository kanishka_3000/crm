/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef BASICSERVICESELECTOR_H
#define BASICSERVICESELECTOR_H

#include <QWidget>
#include <GenericCtrl.h>
#include <ui_BasicServiceSelector.h>
#include <ServiceGroup.h>
#include <Service.h>
class BasicServiceSelector : public GenericCtrl,public Ui::BasicServiceSelector
{
    Q_OBJECT

public:
    explicit BasicServiceSelector(QWidget *parent = 0);
    ~BasicServiceSelector();
    void OnCreateCtrl();
public slots:
    void OnServiceTypeSelected(QString sServiceGroup);
    void OnServiceSelected(QString sService);
signals:
    void NotifiyDataSelected(QList<std::shared_ptr<Entity>> pEntities, bool bSelectNClose);

private:
    QMap<QString, std::shared_ptr<Service>> map_CurrentItems;
    bool b_MultipleItems;
};

#endif // BASICSERVICESELECTOR_H
