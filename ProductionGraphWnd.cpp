/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ProductionGraphWnd.h"
#include <QDebug>
#include <CRMDBFields.h>
#include <Task.h>
#include <Order.h>
#include <Payment.h>
#include <entity/ProductCountDataCollector.h>
#include <entity/ProductPerTimeDataCollector.h>
#include <entity/PaymentPerTimeDataCollector.h>
ProductionGraphWnd::ProductionGraphWnd(MainWnd *parent, QString sTitle) :
    GenericWnd(parent, sTitle)
{
    setupUi(this);
    p_ProductDataCollector = new entity::ProductCountDataCollector;
    p_ProductPerTimeDataCollector = new entity::ProductPerTimeDataCollector;
    p_PaymentPerTimeDataCollector = new entity::PaymentPerTimeDataCollector;
}

ProductionGraphWnd::~ProductionGraphWnd()
{
    delete p_ProductDataCollector;
    delete p_ProductPerTimeDataCollector;
    delete p_PaymentPerTimeDataCollector;
}


void ProductionGraphWnd::OnCreate()
{
    connect(p_Filter, SIGNAL(NotifyFilter(QList<FilterCrieteria>)), this, SLOT(OnFilter(QList<FilterCrieteria>)));
}

void ProductionGraphWnd::OnFilter(QList<FilterCrieteria> lstFilter)
{

    QString sStartDate;
    QString sEndDate;
    QString sProductName ;
    QString sGraphName;
    foreach(FilterCrieteria oFilter, lstFilter)
    {
        QString sData = std::get<0>(oFilter);
        QString sValue = std::get<1>(oFilter);
        if(sData == FILTER_PRODCTION_START_DATE)
        {
            sStartDate = sValue;
        }else if(sData == FILTER_PRODUCTION_END_DATE)
        {
            sEndDate = sValue;
        }else if (sData == FILTER_PRODUCT)
        {
            sProductName = sValue;
        }else if(sData == FILTER_GRAPH)
        {
            sGraphName = sValue;
        }
    }
    QVector<QString> vecInput;
    QList<std::shared_ptr<Entity>> lstTotalTasks;
    entity::SummeryFacilitator* pFacilitator = nullptr;
    if(sGraphName == PRODUCT_COUNT_GRAPH)
    {
        vecInput.push_back(sStartDate);
        vecInput.push_back(sEndDate);
        p_ProductDataCollector->CollectEntityData( vecInput, lstTotalTasks);
        pFacilitator = &o_Countfacilitato;
    }else if(sGraphName == PRODUCTS_PER_TIME)
    {
        vecInput.push_back(sProductName);
        vecInput.push_back(sStartDate);
        vecInput.push_back(sEndDate);
        p_ProductPerTimeDataCollector->CollectEntityData(vecInput, lstTotalTasks);
        pFacilitator = &o_PerTimeFacilitator;
    }else if(sGraphName == PAYMENT_GRAPH)
    {
        vecInput.push_back(sStartDate);
        vecInput.push_back(sEndDate);
        p_PaymentPerTimeDataCollector->CollectEntityData(vecInput, lstTotalTasks);
        pFacilitator = &o_PaymentTimeFacilitator;
    }

    QMap<QString, std::shared_ptr<entity::EntitySummery> > mapSummeries;
    QVector<std::shared_ptr<graphs::BarGraphsPoint>> vecBarGraphItems;
    entity::EntityDataSummerizer oSummerizer(*pFacilitator);
     oSummerizer.Summerize(lstTotalTasks, mapSummeries);
    for(auto itr = mapSummeries.begin(); itr!= mapSummeries.end(); itr++)
    {
        std::shared_ptr<BarGraphDataItem> pBarItem = std::dynamic_pointer_cast<BarGraphDataItem>(itr.value());
        vecBarGraphItems.push_back(pBarItem);
    }

    p_BarGraph->AddGraphPoints(vecBarGraphItems);

}


int BarGraphDataItem::GetCount()
{
    return i_Count;
}

QString BarGraphDataItem::GetTitle()
{
    return s_Title;
}


std::shared_ptr<entity::EntitySummery> ProductCountSummeryFacilitator::Create()
{
    return std::shared_ptr<entity::EntitySummery>(new BarGraphDataItem());
}

void ProductCountSummeryFacilitator::GetNameCount(std::shared_ptr<Entity> pEntity, QString &sKey, int &iCount)
{
    std::shared_ptr<Task> pTask = std::static_pointer_cast<Task>(pEntity);
    pTask->ResolveServices();
    std::shared_ptr<Service> pService = pTask->GetService();

    sKey = pService->GetServiceName();
    iCount = pTask->GetCount();
}


std::shared_ptr<entity::EntitySummery> ProductPerTimeFacilitator::Create()
{
   return std::shared_ptr<entity::EntitySummery>(new BarGraphDataItem());
}

void ProductPerTimeFacilitator::GetNameCount(std::shared_ptr<Entity> pEntity, QString &sKey, int &iCount)
{

    std::shared_ptr<Task> pTask = std::static_pointer_cast<Task>(pEntity);
    iCount = pTask->GetCount();
    QString sDate = pTask->GetDeliveryDate();
    if(sDate.isEmpty())
    {
        sKey = "N/A";
        return;
    }

    QStringList oDateSegments = sDate.split("-");
    sKey = QString(oDateSegments.at(0) +QString("/") +oDateSegments.at(1));
}


std::shared_ptr<entity::EntitySummery> PaymentPerTimeFacilitator::Create()
{
    return std::shared_ptr<entity::EntitySummery>(new BarGraphDataItem());
}

void PaymentPerTimeFacilitator::GetNameCount(std::shared_ptr<Entity> pEntity, QString &sKey, int &iCount)
{
    std::shared_ptr<Payment> pPayment = std::static_pointer_cast<Payment>(pEntity);
    iCount = pPayment->GetAmount();

    QString sDate = pPayment->GetData(FDNM_CRM_PAYMENT_DATE).toString();
    QStringList oDateSegments = sDate.split("-");
    if(oDateSegments.size() != 3)
    {
        //Incorrect date format should be yyyy-mm-dd
        //Database formats should be correct, defensive check to avoid crash
        sKey = "Invalid Date";
        return;
    }
    sKey = QString(oDateSegments.at(0) +QString("/") +oDateSegments.at(1));
}
