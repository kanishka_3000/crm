/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "Payment.h"

Payment::Payment()
{
    s_PersistatName = TBLNM_CRM_PAYMENT;
    s_DefName = TBLNM_CRM_PAYMENT;
    s_Key = FDNM_CRM_PAYMENT_PAYMENTID;
}

double Payment::GetAmount()
{
    return GetData(FDNM_CRM_PAYMENT_AMOUNT).toDouble();
}

