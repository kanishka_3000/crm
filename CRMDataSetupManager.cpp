/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "CRMDataSetupManager.h"
#include <CRMDBFields.h>
CRMDataSetupManager::CRMDataSetupManager()
{

}

void CRMDataSetupManager::Setup(Connection *pConnection, Logger *pLogger)
{
    DataSetupManager::Setup(pConnection, pLogger);

    AddUserDataTable(TBLNM_CRM_CUSTOMER);
    AddUserDataTable(TBLNM_CRM_SERVICEGROUP);
    AddUserDataTable(TBLNM_CRM_SERVICES);
    AddUserDataTable(TBLNM_CRM_TASK);
    AddUserDataTable(TBLNM_CRM_CLIENTORDER);
    AddUserDataTable(TBLNM_CRM_PAYMENT);
}
