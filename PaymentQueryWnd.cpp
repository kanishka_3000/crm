/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "PaymentQueryWnd.h"
#include <CRMDBFields.h>
PaymentQueryWnd::PaymentQueryWnd(MainWnd *parent,QString sTitle) :
    GenericWnd(parent, sTitle)
{
    setupUi(this);
    p_QueryCtrl->SetKeyName(FDNM_CRM_PAYMENT_PAYMENTID);
    p_QueryCtrl->SetConfigName("PaymentWnd");
    p_QueryCtrl->SetTableName(TBLNM_CRM_PAYMENT);
    p_QueryCtrl->SetEnableEdit(false);
    p_QueryCtrl->SetEnableNew(false);
    p_QueryCtrl->SetEnabledSummery(FDNM_CRM_PAYMENT_AMOUNT, "Total");
}


void PaymentQueryWnd::OnCreate()
{
    p_QueryCtrl->Reload();
    connect(p_FilterCtrl, SIGNAL(NotifyFilter(QList<FilterCrieteria>)),p_QueryCtrl , SLOT(OnFilter(QList<FilterCrieteria>)));
}
