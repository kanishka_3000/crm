/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ADDSERVICESWND_H
#define ADDSERVICESWND_H

#include <QWidget>
#include "ui_AddServicesWnd.h"
#include <GenericWnd.h>
#include <ServiceGroup.h>
#include <Service.h>
class AddServicesWnd : public GenericWnd, public Ui::AddServicesWnd
{
    Q_OBJECT

public:
    explicit AddServicesWnd(MainWnd* pMainWnd, QString sTitle);
    void SetServiceGroup(std::shared_ptr<ServiceGroup> pServiceGroup);
    void SetCallback(QWidget* pWnd);
    ~AddServicesWnd();
    void OnEdit(std::shared_ptr<Service> pService);
    void OnCopy(std::shared_ptr<Service> pService);
private:

};

#endif // ADDSERVICESWND_H
