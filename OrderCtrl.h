/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ORDERCTRL_H
#define ORDERCTRL_H

#include <QWidget>
#include "ui_OrderCtrl.h"
#include <GenericCtrl.h>
#include <EntityManipulaterCtrl.h>
#include <Customer.h>
#include <QSqlTableModel>
#include <GenericTableModel.h>
#include <ServiceDataHandler.h>
#include <Order.h>
#include <Task.h>
#include <set>
#include <FieldCompleter.h>
#include <EntitiyReporableModel.h>
#include <reporting/ReportManager.h>
using namespace std;
class OrderCtrl : public EntityManipulaterCtrl,public QueryCtrlCommandHandler, public reporting::ReportDataProvider,public Ui::OrderCtrl
{
    Q_OBJECT

public:
    class OrderCtrlSaveDislpayer: public EntityManipulaterSaveDislpayer
    {
    public:
        OrderCtrlSaveDislpayer(QLabel* pOutput, QWidget* parent);
        virtual void EntitySaved(bool bEdit) override;
    private:
        QLabel* p_Label;
        QWidget* p_Parent;

        // EntityManipulaterSaveDislpayer interface
    public:
        virtual void CustomizeExistMsg(QString &sMsg) override;
    };
    class OrderTaskDependacyResolver: public EntityDataResolver
    {
    public:
         virtual void Resolve(std::shared_ptr<EntityReportData> pData) override;
    };

    explicit OrderCtrl(QWidget *parent = 0);
    ~OrderCtrl();
    virtual void OnCreateCtrl();
    void InitializeWithCustomer(std::shared_ptr<Customer> pCustomer);
    void EditOrder(std::shared_ptr<Order> pOrder);
    void OpenIndependant();
    void AdjustPaymentQuery();

    //Reporting
    virtual QVariant ProvideReportData(QString sFieldName) override;
    //Reporting

    void InitializeReportManager();
public:
    virtual void UpdateEditCommandName(QString &rEditButtnName) override;
    virtual void UpdateNewCommandName(QString &rNewCommandName) override;
protected:
    void SetTotalOrderValue();
    void CalculateTotalPayable();
    bool CustomValidations(QString &sError);
    void InitializePaymentQuery();
    void CreateDirectories();
public slots:
    void OnAddServices();
    void OnServicesSelected(QList<std::shared_ptr<Entity>> pEntities, bool bSelectNClose);
    void OnRemove();
    void OnSave();
    void OnModelChanged(QModelIndex oBegin, QModelIndex oEnd);
    void OnMakePayment();
    void OnPayment();
    void OnPrint();
    void OnDelivered();
    void OnPaymentEdit(QString sPaymentID);
private:
    std::shared_ptr<Order> p_Order;
    QSqlTableModel* p_UserTable;
    EntitiyReporableModel* p_ServiceModel;
    ServiceDataHandler* p_ServiceDataHandler;
    WindowConfig oConfig;
    std::set<std::shared_ptr<Task>> set_OriginalTasks;
    FieldCompleter* p_Clm;
    bool b_OrderSaved;
    double d_OrderValue;
    double d_PayedAmount;
    double d_Payable;
    reporting::ReportManager* p_ReportManager;
    OrderTaskDependacyResolver o_Resolver;

    // QueryCtrlCommandHandler interface

};

#endif // ORDERCTRL_H
