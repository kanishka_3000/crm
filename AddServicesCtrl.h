/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ADDSERVICESCTRL_H
#define ADDSERVICESCTRL_H

#include <QWidget>
#include "ui_AddServicesCtrl.h"
#include <GenericCtrl.h>
#include <EntityManipulaterCtrl.h>
#include <QSqlTableModel>
#include <Service.h>
#include <ServiceGroup.h>
class AddServicesCtrl : public EntityManipulaterCtrl, public Ui::AddServicesCtrl
{
    Q_OBJECT

public:
    explicit AddServicesCtrl(QWidget *parent = 0);
    ~AddServicesCtrl();
    void OnCreateCtrl();
    void SetServiceGroup(std::shared_ptr<ServiceGroup> pServiceGroup);
    void OpenWithService(std::shared_ptr<Service> pService);
     virtual void PopulateWithEntity(std::shared_ptr<Entity> pEntity, bool bEditMode = true);
    bool CustomValidations(QString &sError);
private:
    QSqlTableModel* p_TableModel;
};

#endif // ADDSERVICESCTRL_H
