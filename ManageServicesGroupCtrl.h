/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGESERVICESGROUPCTRL_H
#define MANAGESERVICESGROUPCTRL_H

#include <QWidget>
#include "ui_ManageServicesGroupCtrl.h"
#include <QueryCtrl.h>
class ManageServicesGroupCtrl : public GenericCtrl, public Ui::ManageServicesGroupCtrl
{
    Q_OBJECT

public:
    explicit ManageServicesGroupCtrl(QWidget* pParent);
    ~ManageServicesGroupCtrl();

private:


};

#endif // MANAGESERVICESGROUPCTRL_H
