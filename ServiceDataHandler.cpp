/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ServiceDataHandler.h"
#include <Task.h>
#include <QDebug>
#include <Util.h>
ServiceDataHandler::ServiceDataHandler()
{

}

void ServiceDataHandler::GetRow(const WindowConfig *pWindowConfig, std::shared_ptr<void> pData, QVector<QVariant> &rData)
{
    (void)pWindowConfig;
    std::shared_ptr<Task> pTask = std::static_pointer_cast<Task>(pData);
    std::shared_ptr<Service> pService = pTask->GetService();

    rData << pService->GetServiceName();
   // rData << pService->GetServiceDescription();
    rData << Util::DoubleToString(pService->GetValue());
    rData << pTask->GetCount();
    rData << Util::DoubleToString((pTask->GetTotalValue()));
}

void ServiceDataHandler::OnDataChanged(const WindowConfig *pWindowConfig, int iColumn, std::shared_ptr<void> pData,QVariant oValue)
{
    (void)pWindowConfig;

    if(iColumn != VALUE_FIELD)
        return;

    std::shared_ptr<Task> pTask = std::static_pointer_cast<Task>(pData);
    pTask->SetCount(oValue.toInt());
    pTask->SetTotalValue();

}

