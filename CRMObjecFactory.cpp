#include "CRMObjecFactory.h"
#include <CRMMainWnd.h>
#include <CRMEntityFactory.h>
#include <CRMDataSetupManager.h>
CRMObjecFactory::CRMObjecFactory()
{

}

MainWnd *CRMObjecFactory::CreateMainWnd()
{
    return new CRMMainWnd();
}

EntityFactory *CRMObjecFactory::CreateEntityFactory()
{
    return new CRMEntityFactory();
}



DataSetupManager *CRMObjecFactory::GetSetupManager()
{
    return new CRMDataSetupManager();
}
