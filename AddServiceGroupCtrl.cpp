#include "AddServiceGroupCtrl.h"
#include <crmglobaldefs.h>
#include <GenericWnd.h>
AddServiceGroupCtrl::AddServiceGroupCtrl(QWidget *parent) :
   EntityManipulaterCtrl(parent)
{
    setupUi(this);
    b_DeleteWhenDone = false;
    SetEntityType(ENTITY_SERVICE_GROUP);
}

AddServiceGroupCtrl::~AddServiceGroupCtrl()
{

}

void AddServiceGroupCtrl::OnCreateCtrl()
{
    FillComboWithEnum(cmb_State,E_STATE);
    btn_Close->setVisible(false);
}

void AddServiceGroupCtrl::PopulateWithEntity(std::shared_ptr<Entity> pEntity)
{
    EntityManipulaterCtrl::PopulateWithEntity(pEntity);
    btn_Close->setVisible(true);
}

void AddServiceGroupCtrl::OnPreSave()
{
    if(!b_EditMode)
    {
         p_Entity->ResetPrimaryKey();
    }

}

void AddServiceGroupCtrl::OnClose()
{
    p_Parent->OnChildRemoveRequest(this);
    deleteLater();
}
