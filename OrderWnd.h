/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ORDERWND_H
#define ORDERWND_H

#include <QWidget>
#include <GenericWnd.h>
#include "ui_OrderWnd.h"
#include <Customer.h>
class OrderWnd : public GenericWnd, public Ui::OrderWnd
{
    Q_OBJECT

public:
    explicit OrderWnd(MainWnd* pMainWnd, QString sTitle);
    ~OrderWnd();
    void InitializeWithCustomer(std::shared_ptr<Customer> pCustomer);
    void InitliazeWithOrder(std::shared_ptr<Order> pOrder);
    void OpenIndependant();
    void OnCreate() override;
private:

};

#endif // ORDERWND_H
