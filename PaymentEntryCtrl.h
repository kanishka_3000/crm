/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef PAYMENTENTRYCTRL_H
#define PAYMENTENTRYCTRL_H

#include <QWidget>
#include <EntityManipulaterCtrl.h>
#include "ui_PaymentEntryCtrl.h"
#include <Order.h>
class PaymentEntryCtrl : public EntityManipulaterCtrl, public Ui::PaymentEntryCtrl
{
    Q_OBJECT

public:
    explicit PaymentEntryCtrl(QWidget *parent = 0);
    ~PaymentEntryCtrl();

    void OnCreateCtrl();
    void InitalizeWithOrder(std::shared_ptr<Order> pOrder);
    bool OnSave(EntityManipulaterSaveDislpayer *pSaveDisplayer);
private:

signals:
     void NotifyPayment();

};

#endif // PAYMENTENTRYCTRL_H
