/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "PaymentEntryCtrl.h"
#include <CRMDBFields.h>
#include <GenericWnd.h>
PaymentEntryCtrl::PaymentEntryCtrl(QWidget *parent) :
EntityManipulaterCtrl(parent)
{
    setupUi(this);
    SetEntityType(TBLNM_CRM_PAYMENT);
    SetFieldProperty(txt_OrderID, FDNM_CRM_PAYMENT_ORDERID);
    SetFieldProperty(txt_PaymentDate, FDNM_CRM_PAYMENT_DATE);
    SetFieldProperty(txt_Reason, FDNM_CRM_PAYMENT_REASON);
    SetFieldProperty(txt_TakenBy, FDNM_CRM_PAYMENT_TAKENBY);
    SetFieldProperty(spin_Amount, FDNM_CRM_PAYMENT_AMOUNT);
    b_DeleteWhenDone = true;
}

PaymentEntryCtrl::~PaymentEntryCtrl()
{

}

void PaymentEntryCtrl::OnCreateCtrl()
{
    QDate oDate = QDate::currentDate();
    QString sDate = oDate.toString("yyyy-MM-dd");
    txt_PaymentDate->setText(sDate);
    txt_TakenBy->setText(p_Parent->GetApplication()->GetUserName());
    connect(btn_Submit,SIGNAL(clicked(bool)),this, SLOT(OnSave()));
}

void PaymentEntryCtrl::InitalizeWithOrder(std::shared_ptr<Order> pOrder)
{
    txt_OrderID->setText(pOrder->GetOrderID());
}

bool PaymentEntryCtrl::OnSave(EntityManipulaterSaveDislpayer *pSaveDisplayer)
{
   bool bReturn =  EntityManipulaterCtrl::OnSave(pSaveDisplayer);
   if(bReturn)
   {
       NotifyPayment();
   }
   p_Parent->RemoveWindow();
   return bReturn;
}
