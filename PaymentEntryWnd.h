/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef PAYMENTENTRYWND_H
#define PAYMENTENTRYWND_H

#include <QWidget>
#include "ui_PaymentEntryWnd.h"
#include <GenericWnd.h>
#include <Payment.h>
class PaymentEntryWnd : public GenericWnd, public Ui::PaymentEntryWnd
{
    Q_OBJECT

public:
    explicit PaymentEntryWnd(MainWnd* pMainWnd, QString sTitle);
    ~PaymentEntryWnd();
     void InitalizeWithOrder(std::shared_ptr<Order> pOrder);
     void OnCreate();
     void PopulateWithPayment(std::shared_ptr<Payment> pPayment);
     void SetPaymentNotifier(GenericCtrl *pNotifier);
private:

};

#endif // PAYMENTENTRYWND_H
