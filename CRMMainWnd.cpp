#include "CRMMainWnd.h"
#include <CRMApplication.h>
#include <crmglobaldefs.h>
#include <ServiceSelector.h>
#include <OrderWnd.h>
#include <ManageCustomerWnd.h>
#include <Role.h>
#include <User.h>
CRMMainWnd::CRMMainWnd(QWidget *parent)
    : MainWnd(parent)
{
    connect(p_ActManage_Customers,SIGNAL(triggered(bool)), this, SLOT(OnMenuManageCustomers()));
    connect(p_ActManage_Service_Groups,SIGNAL(triggered(bool)), this,SLOT(OnMenuManageServiceGroups()));
    connect(p_ActManaage_Services,SIGNAL(triggered(bool)), this,SLOT(OnMenuManageServices()));
    connect(p_ActOrders,SIGNAL(triggered(bool)), this,SLOT(OnMenuOrders()));
    connect(p_ActNew_Order,SIGNAL(triggered(bool)), this,SLOT(OnMenuNewOrder()));
    connect(p_ActNew_Customer, SIGNAL(triggered(bool)),this,SLOT(OnMenuNewCustomer()));
    connect(p_ActProduction, SIGNAL(triggered(bool)), this, SLOT( OnMenuProduction()));
    connect(p_ActPayment_Query, SIGNAL(triggered(bool)), this, SLOT( OnPaymentQuery()));
}

CRMMainWnd::~CRMMainWnd()
{

}

void CRMMainWnd::Initilize(Application *pApplication)
{
    MainWnd::Initilize(pApplication);

    std::shared_ptr<Role> pRole = pApplication->GetUser()->p_Role;
    if(!pRole->HasPriviledge(PRI_ADD_USERS))
    {
        p_ActManage_Service_Groups->setEnabled(false);
    }
}


void CRMMainWnd::OnMenuManageCustomers()
{
    p_Application->CreateWnd(WND_MNG_CUSTOMERS);
}

void CRMMainWnd::OnMenuManageServiceGroups()
{
    p_Application->CreateWnd(WND_ADD_SERVICE_GROUP);
}

void CRMMainWnd::OnMenuManageServices()
{
    ServiceSelector* pServiceSelector = static_cast<ServiceSelector*>(p_Application->CreateWnd(WND_SERVICE_SELECTOR));
    pServiceSelector->Populate(true);
}

void CRMMainWnd::OnMenuOrders()
{
    p_Application->CreateWnd(WND_ORDER_QUERy);
}

void CRMMainWnd::OnMenuNewOrder()
{
    OrderWnd* pWnd = static_cast<OrderWnd*>(p_Application->CreateWnd(WND_ORDER));
    pWnd->OpenIndependant();
}

void CRMMainWnd::OnMenuNewCustomer()
{
     ManageCustomerWnd* pWnd = static_cast<ManageCustomerWnd*>(p_Application->CreateWnd(WND_MNG_CUSTOMERS));
     pWnd->AdjustOpenType(ManageCustomerWnd::OpenType::NEW);

}

void CRMMainWnd::OnMenuProduction()
{
    p_Application->CreateWnd(WND_PRODUCTION_GRAPH);
}

void CRMMainWnd::OnPaymentQuery()
{
    p_Application->CreateWnd(WND_PAYMENT_QUERY);
}
