#include "CRMApplication.h"
#include <CustomerRegisterWnd.h>
#include <crmglobaldefs.h>
#include <ManageCustomerWnd.h>
#include <AddServiceGroupWnd.h>
#include <ManageServicesGroupWnd.h>
#include <AddServicesWnd.h>
#include <ServiceSelector.h>
#include <OrderWnd.h>
#include <OrderQueryWnd.h>
#include <PaymentEntryWnd.h>
#include <NotificationWnd.h>
#include <OrderNotificationResolver.h>
#include <ProductionGraphWnd.h>
#include <PaymentQueryWnd.h>
#include <CRMPreferenceWnd.h>
CRMApplication::CRMApplication(QObject *parent)
{
    (void)parent;
}

void CRMApplication::RegisterWindows()
{
    Application::RegisterWindows();
    SetWindowHandler<ManageCustomerWnd>(WND_MNG_CUSTOMERS,"Manage Customers", i_MaxWndInstances);
    SetWindowHandler<CustomerRegisterWnd>(WND_ADD_CUSTOMERS, "Add Customers", i_MaxWndInstances);
    SetWindowHandler<ManageServicesGroupWnd>(WND_ADD_SERVICE_GROUP, "Manage Service Group", i_MaxWndInstances);
    SetWindowHandler<ServiceSelector>(WND_SERVICE_SELECTOR,"Services", i_MaxWndInstances);
    SetWindowHandler<AddServicesWnd>(WND_ADE_SERVICE,"Add New Service", i_MaxWndInstances);
    SetWindowHandler<OrderWnd>(WND_ORDER,"New Order", i_MaxWndInstances);
    SetWindowHandler<OrderQueryWnd>(WND_ORDER_QUERy, "Manage Orders", i_MaxWndInstances);
    SetWindowHandler<PaymentEntryWnd>(WND_PAYMENT_ENTRY, "Payment Entry", 1);
    SetWindowHandler<ProductionGraphWnd>(WND_PRODUCTION_GRAPH, "Production Graph", 1);
    SetWindowHandler<PaymentQueryWnd>(WND_PAYMENT_QUERY, "Payment Query", i_MaxWndInstances);
    SetWindowHandler<CRMPreferenceWnd>(WND_PREF, "Preference Wnd", 1);
}





void CRMApplication::CustomizeDefaultWindows(GenericWnd *pWnd, int iType)
{
    switch(iType)
    {
    case WND_NOTIFICATIONS:
    {
        NotificationWnd* pNotificationWnd = static_cast<NotificationWnd*>(pWnd);
        pNotificationWnd->Initialize(new OrderNotificationResolver());
        break;
    }
    }
}

QString CRMApplication::GetApplicationName()
{
    return "Lanka Edge Data Management Demo";
}
