/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ORDERQUERYWND_H
#define ORDERQUERYWND_H

#include <QWidget>
#include <GenericWnd.h>
#include "ui_OrderQueryWnd.h"
#include <GenericFilterCtrl.h>
class OrderQueryWnd : public GenericWnd, public QueryCtrlCommandHandler, public Ui::OrderQueryWnd
{
    Q_OBJECT

public:
    explicit OrderQueryWnd(MainWnd* pMainWnd, QString sTitle);
    void SetFilter(QMap<QString,QString> mapFieldData);
    virtual void OnCreate() override;

    ~OrderQueryWnd();
public slots:
     void OnNewOrder();
     void OnEditRequest(QString sKey);
private:


     // QueryCtrlCommandHandler interface
public:
     virtual void UpdateEditCommandName(QString &rEditButtnName) override;
     virtual void UpdateNewCommandName(QString &rNewCommandName) override;
     virtual int GetCommandCount() override{return 0;}
     virtual QAction *GetAction(int iActionNo) override {(void)iActionNo;return nullptr;}
     virtual void FixButton(int iActionNo, QPushButton *pButton) override {(void)iActionNo;(void)pButton;}
};



#endif // ORDERQUERYWND_H
