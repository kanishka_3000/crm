/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ORDERFILTERCTRL_H
#define ORDERFILTERCTRL_H

#include <QWidget>
#include <GenericFilterCtrl.h>
#include "ui_OrderFilterCtrl.h"
#include <FieldCompleter.h>
class OrderFilterCtrl : public GenericFilterCtrl,public Ui::OrderFilterCtrl
{
    Q_OBJECT

public:
    explicit OrderFilterCtrl(QWidget *parent = 0);
    ~OrderFilterCtrl();
    void OnCreateCtrl();
protected:
    void SetClearButton(QPushButton* pClearButton);
public slots:
    void OnApply();

private:
    FieldCompleter* p_Clm;
};

#endif // ORDERFILTERCTRL_H
