#include "CustomerRegisterCtrl.h"
#include <GenericWnd.h>
#include <crmglobaldefs.h>
#include <QIntValidator>
#include <QRegExpValidator>
#include <LabelAnimator.h>
#include <QMessageBox>
#include <GenericWnd.h>
#include <OrderWnd.h>
CustomerRegisterCtrl::CustomerRegisterCtrl(QWidget *parent) :
    EntityManipulaterCtrl(parent)

{
    setupUi(this);
    b_DeleteWhenDone = false;
    s_EntityType = ENTITY_CUSTOMER;
    btn_Close->setVisible(false);   

}

CustomerRegisterCtrl::~CustomerRegisterCtrl()
{

}

void CustomerRegisterCtrl::OnCreateCtrl()
{
    SetCurrentDateToAllFields();
    txt_RegistedBy->setText(p_Parent->GetApplication()->GetUserName());
    txt_RegistedBy->setReadOnly(true);
    dt_RegistratinDate->setReadOnly(true);
    txt_ContactNo->setValidator(new QIntValidator(111111111,999999999,txt_ContactNo));\
    connect(btn_Close, SIGNAL(clicked(bool)),this,SLOT(OnClose()));

}

void CustomerRegisterCtrl::PopulateWithEntity(std::shared_ptr<Entity> pEntity)
{
    EntityManipulaterCtrl::PopulateWithEntity(pEntity);
    btn_Close->setVisible(true);
}

bool CustomerRegisterCtrl::CustomValidations(QString &sError)
{
    if(txt_ContactNo->text().length() < 9)
    {
        sError = tr("Invalid Contact No Length");
        return false;
    }
    return true;
}

void CustomerRegisterCtrl::OnClose()
{
    p_Parent->OnChildRemoveRequest(this);
    deleteLater();
}

bool CustomerRegisterCtrl::OnSave(EntityManipulaterSaveDislpayer *pSaveDisplayer)
{
    (void)pSaveDisplayer;
    CustomerRegisterCallback oReg(lbl_Notification,this);
    return EntityManipulaterCtrl::OnSave(&oReg);
}


void CustomerRegisterCtrl::CustomerRegisterCallback::EntitySaved(bool bEdit)
{
    (void)bEdit;
    p_Label->setText(tr("Customer saved successfully"));
    LabelAnimator oAnim(p_Label,p_Parent);
    oAnim.Animate(LabelAnimator::AnimationType::FADEOUT);

    if(!bEdit)
    {

        QMessageBox::StandardButton reply = QMessageBox::question(p_Parent, tr("Order?"), tr("Registration Done.  Would you like to place an order?"),  QMessageBox::Yes|QMessageBox::No , QMessageBox::StandardButton::Yes);
        if(reply == QMessageBox::Yes)
        {
            OrderWnd* pOrderWnd = static_cast<OrderWnd*>(Application::GetApplication()->CreateWnd(WND_ORDER));
            std::shared_ptr<Customer> pCustomer = std::static_pointer_cast<Customer>(p_Parent->p_Entity);
            pOrderWnd->InitializeWithCustomer(pCustomer);
        }
    }
    else
    {
        QMessageBox::information(p_Parent, tr("Edit Success"),  tr("Customer details updated"));
    }
}
