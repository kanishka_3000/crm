/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef PAYMENT_H
#define PAYMENT_H
#include <Entity.h>
#include <CRMDBFields.h>
class Payment: public Entity
{
public:
    Payment();
    double GetAmount();
};

#endif // PAYMENT_H
