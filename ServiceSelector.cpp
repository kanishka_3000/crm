/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ServiceSelector.h"
#include <crmglobaldefs.h>
#include <AddServicesWnd.h>
#include <Service.h>
ServiceSelector::ServiceSelector(MainWnd* pMainWnd, QString sTitle) :
    EntityGroupTableWnd(pMainWnd, sTitle)

{
    setMinimumSize(500,400);
    setWindowTitle("Service Selection");
    b_Populated = false;
}

ServiceSelector::~ServiceSelector()
{

}

void ServiceSelector::Populate(bool bAceptChanges )
{
    if(b_Populated)
        return;
    EntityGroupTableWnd::OnCreate();
    o_Sel.s_GroupEntity = ENTITY_SERVICE_GROUP;
    o_Sel.s_FLDGroupID = FLD_SG_ID;
    o_Sel.s_FLDGroupDescription = "name";

    o_Sel.s_DataTable = ENTITY_SERVICE;
    o_Sel.s_DataTableKey = FLD_SERVICE_ID;
    o_Sel.s_GroupColumn = "groupid";

    Initialize(o_Sel,bAceptChanges);
    b_Populated = true;
    //ReSize(480, 392);
}

void ServiceSelector::OnCreate()
{
   // Populate();

}

void ServiceSelector::OnAddNew(std::shared_ptr<Entity> pGroup)
{
    GenericWnd* pWnd =  p_Application->CreateWnd(WND_ADE_SERVICE);
    AddServicesWnd* pGrWnd = static_cast<AddServicesWnd*>(pWnd);
    std::shared_ptr<ServiceGroup> pSGroup = std::static_pointer_cast<ServiceGroup>(pGroup);
    pGrWnd->SetCallback(this);
    pGrWnd->SetServiceGroup(pSGroup);

}

void ServiceSelector::OnEdit(std::shared_ptr<Entity> pEntity)
{
    GenericWnd* pWnd =  p_Application->CreateWnd(WND_ADE_SERVICE);
    AddServicesWnd* pGrWnd = static_cast<AddServicesWnd*>(pWnd);
    std::shared_ptr<Service> pService = std::static_pointer_cast<Service>(pEntity);
    pGrWnd->SetCallback(this);
    pGrWnd->OnEdit(pService);
}

void ServiceSelector::OnCopy(std::shared_ptr<Entity> pEntity)
{
    GenericWnd* pWnd =  p_Application->CreateWnd(WND_ADE_SERVICE);
    AddServicesWnd* pGrWnd = static_cast<AddServicesWnd*>(pWnd);
    std::shared_ptr<Service> pService = std::static_pointer_cast<Service>(pEntity);
    pGrWnd->SetCallback(this);
    pGrWnd->OnCopy(pService);
}

void ServiceSelector::OnServiceAdded(bool bEdit)
{
    (void)bEdit;
    Reload();
}
