/***************************This is auto generated************************************
***************Kanishka Weerasekara (c)-- kkgweerasekara@gmail.com*******************
*************************************************************************************/
#ifndef __CRMDB_FIELDS__ 
#define __CRMDB_FIELDS__ 
//Table name for ROLE
#define TBLNM_CRM_ROLE	"Role" 
//Table fields for ROLE 
#define FDNM_CRM_ROLE_ROLENAME 	 "rolename"
#define FDNM_CRM_ROLE_ROLEDESCRIPTION 	 "roledescription"
//Table name for PRIVILEDGE
#define TBLNM_CRM_PRIVILEDGE	"Priviledge" 
//Table fields for PRIVILEDGE 
#define FDNM_CRM_PRIVILEDGE_PRICODE 	 "pricode"
#define FDNM_CRM_PRIVILEDGE_NAME 	 "name"
#define FDNM_CRM_PRIVILEDGE_PREVDESCRIPTION 	 "prevdescription"
//Table name for LOGIN
#define TBLNM_CRM_LOGIN	"Login" 
//Table fields for LOGIN 
#define FDNM_CRM_LOGIN_USERNAME 	 "username"
#define FDNM_CRM_LOGIN_PASSWORD 	 "password"
#define FDNM_CRM_LOGIN_NAME 	 "name"
#define FDNM_CRM_LOGIN_ROLENAME 	 "rolename"
//Table name for ROLE_PRIVILEDGE
#define TBLNM_CRM_ROLE_PRIVILEDGE	"Role_Priviledge" 
//Table fields for ROLE_PRIVILEDGE 
#define FDNM_CRM_ROLE_PRIVILEDGE_ROLENAME 	 "rolename"
#define FDNM_CRM_ROLE_PRIVILEDGE_PRICODE 	 "pricode"
//Table name for CUSTOMER
#define TBLNM_CRM_CUSTOMER	"Customer" 
//Table fields for CUSTOMER 
#define FDNM_CRM_CUSTOMER_PHONE 	 "phone"
#define FDNM_CRM_CUSTOMER_NAME 	 "name"
#define FDNM_CRM_CUSTOMER_ADDRESS 	 "address"
#define FDNM_CRM_CUSTOMER_EMAIL 	 "email"
#define FDNM_CRM_CUSTOMER_OTHER 	 "other"
#define FDNM_CRM_CUSTOMER_DATEOFBIRTH 	 "dateofbirth"
#define FDNM_CRM_CUSTOMER_NID 	 "nid"
#define FDNM_CRM_CUSTOMER_REGISTRATIONDATE 	 "registrationdate"
#define FDNM_CRM_CUSTOMER_REGISTEDBY 	 "registedby"
//Table name for SERVICEGROUP
#define TBLNM_CRM_SERVICEGROUP	"ServiceGroup" 
//Table fields for SERVICEGROUP 
#define FDNM_CRM_SERVICEGROUP_SERVICEGROUPID 	 "servicegroupid"
#define FDNM_CRM_SERVICEGROUP_NAME 	 "name"
#define FDNM_CRM_SERVICEGROUP_DESCRIPTION 	 "description"
#define FDNM_CRM_SERVICEGROUP_STATE 	 "state"
//Table name for SQLITE_SEQUENCE
#define TBLNM_CRM_SQLITE_SEQUENCE	"sqlite_sequence" 
//Table fields for SQLITE_SEQUENCE 
#define FDNM_CRM_SQLITE_SEQUENCE_NAME 	 "name"
#define FDNM_CRM_SQLITE_SEQUENCE_SEQ 	 "seq"
//Table name for SERVICES
#define TBLNM_CRM_SERVICES	"Services" 
//Table fields for SERVICES 
#define FDNM_CRM_SERVICES_SERVICEID 	 "serviceid"
#define FDNM_CRM_SERVICES_SERVICENAME 	 "servicename"
#define FDNM_CRM_SERVICES_SERVICEDESCRIPTION 	 "servicedescription"
#define FDNM_CRM_SERVICES_STATE 	 "state"
#define FDNM_CRM_SERVICES_VALUE 	 "value"
#define FDNM_CRM_SERVICES_GROUPID 	 "groupid"
//Table name for TASK
#define TBLNM_CRM_TASK	"Task" 
//Table fields for TASK 
#define FDNM_CRM_TASK_TASKID 	 "taskid"
#define FDNM_CRM_TASK_ORDERID 	 "orderid"
#define FDNM_CRM_TASK_SERVICEID 	 "serviceid"
#define FDNM_CRM_TASK_COUNT 	 "count"
#define FDNM_CRM_TASK_DELIVERYDATE 	 "deliverydate"
#define FDNM_CRM_TASK_ASSIGNEE 	 "assignee"
//Table name for CLIENTORDER
#define TBLNM_CRM_CLIENTORDER	"ClientOrder" 
//Table fields for CLIENTORDER 
#define FDNM_CRM_CLIENTORDER_ORDERID 	 "orderid"
#define FDNM_CRM_CLIENTORDER_CUSTOMERID 	 "customerid"
#define FDNM_CRM_CLIENTORDER_DELIVERYDATE 	 "deliverydate"
#define FDNM_CRM_CLIENTORDER_ASIGNEE 	 "asignee"
#define FDNM_CRM_CLIENTORDER_TAKENBY 	 "takenby"
#define FDNM_CRM_CLIENTORDER_COMMENT 	 "comment"
#define FDNM_CRM_CLIENTORDER_STATE 	 "state"
//Table name for PAYMENT
#define TBLNM_CRM_PAYMENT	"Payment" 
//Table fields for PAYMENT 
#define FDNM_CRM_PAYMENT_PAYMENTID 	 "paymentid"
#define FDNM_CRM_PAYMENT_ORDERID 	 "orderid"
#define FDNM_CRM_PAYMENT_AMOUNT 	 "amount"
#define FDNM_CRM_PAYMENT_REASON 	 "reason"
#define FDNM_CRM_PAYMENT_TAKENBY 	 "takenby"
#define FDNM_CRM_PAYMENT_DATE 	 "date"
#endif //  __DB_FIELDS__ 
