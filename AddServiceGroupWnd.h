/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ADDSERVICEGROUPWND_H
#define ADDSERVICEGROUPWND_H

#include <QWidget>
#include "ui_AddServiceGroupWnd.h"
#include <GenericWnd.h>
class AddServiceGroupWnd : public GenericWnd,public Ui::AddServiceGroupWnd
{
    Q_OBJECT

public:
    explicit AddServiceGroupWnd(MainWnd *parent ,QString sTitle);
    ~AddServiceGroupWnd();

private:


    // GenericWnd interface
public:
    virtual void OnCreate() override;
};

#endif // ADDSERVICEGROUPWND_H
