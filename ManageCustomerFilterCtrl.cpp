#include "ManageCustomerFilterCtrl.h"
#include <CRMDBFields.h>
#include <GenericWnd.h>
ManageCustomerFilterCtrl::ManageCustomerFilterCtrl(QWidget *parent) :
GenericFilterCtrl(parent)
{
    setupUi(this);
}

ManageCustomerFilterCtrl::~ManageCustomerFilterCtrl()
{

}

void ManageCustomerFilterCtrl::OnCreateCtrl()
{
    txt_Phone->setValidator(new QIntValidator(this));
    SetCurrentDateToAllFields();
    p_Clm = new FieldCompleter(p_Parent->GetApplication(),TBLNM_CRM_CUSTOMER);
    p_Clm->SetSource(txt_CustomerName, FDNM_CRM_CUSTOMER_NAME);
    p_Clm->SetDestination(txt_Phone, FDNM_CRM_CUSTOMER_PHONE);
    chk_RegisterDate->setMinimumHeight(lbl_Email->geometry().height()+ 2);
}
