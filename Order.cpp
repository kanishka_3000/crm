/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "Order.h"
#include <CRMDBFields.h>
#include <iostream>
Order::Order()
{
    s_PersistatName = TBLNM_CRM_CLIENTORDER;
    s_DefName = TBLNM_CRM_CLIENTORDER;
    s_Key = FDNM_CRM_CLIENTORDER_ORDERID;
    p_Customer = nullptr;
    SetKeyAutoIncrement(true);
}

QString Order::GetCustomerID()
{
    return GetData(FDNM_CRM_CLIENTORDER_CUSTOMERID).toString();
}

QString Order::GetOrderID()
{
    return GetData(FDNM_CRM_CLIENTORDER_ORDERID).toString();
}

QString Order::GetDeliveryDate()
{
    return GetData(FDNM_CRM_CLIENTORDER_DELIVERYDATE).toString();
}



std::shared_ptr<Customer> Order::GetCustomer()
{
    if(!p_Customer)
        p_Customer = std::static_pointer_cast<Customer>(Entity::FindInstance(GetCustomerID(),TBLNM_CRM_CUSTOMER));

    return p_Customer;
}



void Order::ResolveReferences()
{
    lst_Task = Entity::FindInstances(FDNM_CRM_TASK_ORDERID, GetOrderID(),TBLNM_CRM_TASK);
    lst_Payment = Entity::FindInstances(FDNM_CRM_PAYMENT_ORDERID, GetOrderID(), TBLNM_CRM_PAYMENT);
}
