/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef PRODUCTIONGRAPHWND_H
#define PRODUCTIONGRAPHWND_H

#include "ui_ProductionGraphWnd.h"
#include <GenericWnd.h>
#include <graphs/BarGraph.h>
#include <entity/EntityDataSummerizer.h>
#include <entity/DataCollector.h>
class BarGraphDataItem: public entity::EntitySummery, public graphs::BarGraphsPoint
{
    // BarGraphsPoint interface
public:
    virtual int GetCount();
    virtual QString GetTitle();
};

class ProductCountSummeryFacilitator: public entity::SummeryFacilitator
{
    // SummeryFacilitator interface
public:
    virtual std::shared_ptr<entity::EntitySummery> Create();
    virtual void GetNameCount(std::shared_ptr<Entity> pEntity, QString& sKey, int& iCount) override;
};

class ProductPerTimeFacilitator: public entity::SummeryFacilitator
{
    // SummeryFacilitator interface
public:
    virtual std::shared_ptr<entity::EntitySummery> Create();
    virtual void GetNameCount(std::shared_ptr<Entity> pEntity, QString &sKey, int &iCount);
};

class PaymentPerTimeFacilitator: public entity::SummeryFacilitator
{

    // SummeryFacilitator interface
public:
    virtual std::shared_ptr<entity::EntitySummery> Create();
    virtual void GetNameCount(std::shared_ptr<Entity> pEntity, QString &sKey, int &iCount);
};

class ProductionGraphWnd : public GenericWnd, private Ui::ProductionGraphWnd
{
    Q_OBJECT

public:
    explicit ProductionGraphWnd(MainWnd *parent ,QString sTitle);
    virtual ~ProductionGraphWnd();
    // GenericWnd interface
public:
    virtual void OnCreate();
public slots:
    void OnFilter(QList<FilterCrieteria> lstFilter);
private:
    entity::DataCollector* p_ProductDataCollector;
    entity::DataCollector* p_ProductPerTimeDataCollector;
    entity::DataCollector* p_PaymentPerTimeDataCollector;

    ProductCountSummeryFacilitator o_Countfacilitato;
    ProductPerTimeFacilitator o_PerTimeFacilitator;
    PaymentPerTimeFacilitator o_PaymentTimeFacilitator;
};

#endif // PRODUCTIONGRAPHWND_H
