#ifndef SERVICEGROUP_H
#define SERVICEGROUP_H
#include <Entity.h>

class ServiceGroup: public Entity
{
public:
    ServiceGroup();
    QString GetServiceGroupName();
    QString GetServiceGroupID();
};

#endif // SERVICEGROUP_H
