/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "PaymentEntryWnd.h"
#include <QDebug>

PaymentEntryWnd::PaymentEntryWnd(MainWnd *pMainWnd, QString sTitle) :
GenericWnd(pMainWnd, sTitle)
{
    setupUi(this);

}

PaymentEntryWnd::~PaymentEntryWnd()
{

}

void PaymentEntryWnd::InitalizeWithOrder(std::shared_ptr<Order> pOrder)
{
    p_Ctrl->InitalizeWithOrder(pOrder);
}

void PaymentEntryWnd::OnCreate()
{
    SetWindowFlags(Qt::Dialog );
    SetWindowModality(Qt::ApplicationModal);
}

void PaymentEntryWnd::PopulateWithPayment(std::shared_ptr<Payment> pPayment)
{
    p_Ctrl->PopulateWithEntity(pPayment);
}

void PaymentEntryWnd::SetPaymentNotifier(GenericCtrl *pNotifier)
{
    connect(p_Ctrl, SIGNAL(NotifyPayment()),pNotifier, SLOT(OnPayment()));
}
