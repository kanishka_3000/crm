#ifndef CRMGLOBALDEFS
#define CRMGLOBALDEFS
#include <globaldefs.h>

#define ENTITY_CUSTOMER "Customer"
#define ENTITY_SERVICE_GROUP "ServiceGroup"
#define ENTITY_SERVICE  "Services"

#define FLD_PHONE  "phone"
#define FLD_SG_ID "servicegroupid"
#define FLD_SERVICE_ID "serviceid"

#define WND_MNG_CUSTOMERS 500
#define WND_ADD_CUSTOMERS 501
#define WND_ADD_SERVICE_GROUP 502
#define WND_SERVICE_SELECTOR  503
#define WND_ADE_SERVICE  504
#define WND_ORDER  505
#define WND_ORDER_QUERy 506
#define WND_PAYMENT_ENTRY  507
#define WND_PRODUCTION_GRAPH  508
#define WND_PAYMENT_QUERY 509

#define SERVICE_OFFSET  100

#define PREF_ROOT "rooddir"
#define PREF_SOURCE "source"
#define PREF_PROCESS "process"
#define PREF_COMPLTE "complete"
#define PREF_CNT_ENABLE "content_enable"

#endif // CRMGLOBALDEFS

