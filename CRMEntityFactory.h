/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef CRMENTITYFACTORY_H
#define CRMENTITYFACTORY_H

#include <EntityFactory.h>
class CRMEntityFactory:public EntityFactory
{
public:
    CRMEntityFactory();

protected:
    virtual void RegisterEntities();
};

#endif // CRMENTITYFACTORY_H
