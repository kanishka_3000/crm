/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ProductCountDataCollector.h"
#include <CRMDBFields.h>
#include <Task.h>
#include <Order.h>
entity::ProductCountDataCollector::ProductCountDataCollector()
{

}



void entity::ProductCountDataCollector::CollectEntityData(QVector<QString> vecInput, QList<std::shared_ptr<Entity> > &lstData)
{
    QString sStartDate = vecInput.at(0);
    QString sEndDate = vecInput.at(1);


    QList<std::shared_ptr<Entity> > lstEntities = Entity::FindInstances(FDNM_CRM_CLIENTORDER_DELIVERYDATE, sStartDate, sEndDate, TBLNM_CRM_CLIENTORDER);
    foreach(std::shared_ptr<Entity> pOrder, lstEntities)
    {
        std::shared_ptr<Order> pOder = std::static_pointer_cast<Order>(pOrder);
        pOder->ResolveReferences();
        QList<std::shared_ptr<Entity>>& lstTaks = pOder->lst_Task;
        lstData += lstTaks;
    }
}
