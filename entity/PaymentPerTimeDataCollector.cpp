/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "PaymentPerTimeDataCollector.h"
#include <Order.h>
entity::PaymentPerTimeDataCollector::PaymentPerTimeDataCollector() :
    entity::DataCollector()
{

}



void entity::PaymentPerTimeDataCollector::CollectEntityData(QVector<QString> vecInput, QList<std::shared_ptr<Entity> > &lstData)
{
    QString sStartDate = vecInput.at(PAYMENT_STDATE_INDEX);
    QString sEndDate = vecInput.at(PAYMENT_ENDDATE_INDEX);

    QList<std::shared_ptr<Entity> > lstEntities = Entity::FindInstances(FDNM_CRM_CLIENTORDER_DELIVERYDATE, sStartDate, sEndDate, TBLNM_CRM_CLIENTORDER);
    foreach(std::shared_ptr<Entity> pOrder, lstEntities)
    {
        std::shared_ptr<Order> pOder = std::static_pointer_cast<Order>(pOrder);
        pOder->ResolveReferences();
        lstData+=pOder->lst_Payment;
    }
}
