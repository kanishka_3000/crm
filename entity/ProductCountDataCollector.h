/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef PRODUCTCOUNTDATACOLLECTOR_H
#define PRODUCTCOUNTDATACOLLECTOR_H

#include <QString>
#include <entity/DataCollector.h>
namespace entity {


class ProductCountDataCollector :public entity::DataCollector
{

public:
    explicit ProductCountDataCollector();
    virtual ~ProductCountDataCollector(){}
signals:

public slots:

    // DataCollector interface
public:
    virtual void CollectEntityData(QVector<QString> vecInput, QList<std::shared_ptr<Entity>>& lstData);

};
}
#endif // PRODUCTCOUNTDATACOLLECTOR_H
