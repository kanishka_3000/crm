/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ProductPerTimeDataCollector.h"
#include <Task.h>
#include <Order.h>
entity::ProductPerTimeDataCollector::ProductPerTimeDataCollector()
{

}



void entity::ProductPerTimeDataCollector::CollectEntityData(QVector<QString> vecInput, QList<std::shared_ptr<Entity> > &lstData)
{
    QString sProductName = vecInput.at(PRDUCT_NAME_INDEX);
    QString sStartDate = vecInput.at(START_DATE_PT_INDEX);
    QString sEndDate = vecInput.at(END_DATE_PT_INDEX);


    QList<std::shared_ptr<Entity> > lstEntities = Entity::FindInstances(FDNM_CRM_CLIENTORDER_DELIVERYDATE, sStartDate, sEndDate, TBLNM_CRM_CLIENTORDER);
    foreach(std::shared_ptr<Entity> pOrder, lstEntities)
    {
        std::shared_ptr<Order> pOder = std::static_pointer_cast<Order>(pOrder);

        pOder->ResolveReferences();
        QList<std::shared_ptr<Entity>>& lstTaks = pOder->lst_Task;
        foreach(std::shared_ptr<Entity> pTask, lstTaks)
        {
            std::shared_ptr<Task> pTs = std::static_pointer_cast<Task>(pTask);
            pTs->ResolveServices();
            if(pTs->p_Service->GetServiceName() == sProductName)
            {
                QString sDeliveryDate = pOder->GetDeliveryDate();
                pTs->SetDeliveryDate(sDeliveryDate);
                lstData.push_back(pTs);
            }
        }
    }
}
