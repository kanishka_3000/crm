/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef PAYMENTPERTIMEDATACOLLECTOR_H
#define PAYMENTPERTIMEDATACOLLECTOR_H


#include <entity/DataCollector.h>
#define PAYMENT_STDATE_INDEX 0
#define PAYMENT_ENDDATE_INDEX 1
namespace entity
{
class PaymentPerTimeDataCollector : public entity::DataCollector
{

public:
    explicit PaymentPerTimeDataCollector();

signals:

public slots:

    // DataCollector interface
public:
    virtual void CollectEntityData(QVector<QString> vecInput, QList<std::shared_ptr<Entity> > &lstData);
};
}
#endif // PAYMENTPERTIMEDATACOLLECTOR_H
