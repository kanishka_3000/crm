/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef PRODUCTPERTIMEDATACOLLECTOR_H
#define PRODUCTPERTIMEDATACOLLECTOR_H

#include <entity/DataCollector.h>
#define PRDUCT_NAME_INDEX  0
#define START_DATE_PT_INDEX 1
#define END_DATE_PT_INDEX  2
namespace entity {


class ProductPerTimeDataCollector: public entity::DataCollector
{
public:
    ProductPerTimeDataCollector();

    // DataCollector interface
public:
    virtual void CollectEntityData(QVector<QString> vecInput, QList<std::shared_ptr<Entity> > &lstData)override;
};
}
#endif // PRODUCTPERTIMEDATACOLLECTOR_H
