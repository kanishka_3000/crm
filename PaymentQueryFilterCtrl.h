/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef PAYMENTQUERYFILTERCTRL_H
#define PAYMENTQUERYFILTERCTRL_H

#include "ui_PaymentQueryFilterCtrl.h"
#include <GenericFilterCtrl.h>
#define FILTER_FROM "filter_from"
#define FILTER_TO "filter_to"
#define FILTER_ORDER_ID "filter_orderid"
class PaymentQueryFilterCtrl : public GenericFilterCtrl, private Ui::PaymentQueryFilterCtrl
{
    Q_OBJECT

public:
    explicit PaymentQueryFilterCtrl(QWidget *parent = 0);

    // GenericCtrl interface
public:
    virtual void OnCreateCtrl();
};

#endif // PAYMENTQUERYFILTERCTRL_H
