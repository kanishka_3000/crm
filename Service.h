/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef SERVICE_H
#define SERVICE_H

#include <Entity.h>
class ServiceGroup;
class Service: public Entity
{
public:
    Service();
    int GetServiceID();
    QString GetServiceName();
    QString GetServiceDescription();
    double GetValue();
    int GetStatus();
    QVariant GetData(QString sFieldName);
    std::shared_ptr<ServiceGroup> GetServiceGroup();
private:
    std::shared_ptr<ServiceGroup> p_ServiceGroup;
};

#endif // SERVICE_H
