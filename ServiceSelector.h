/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef SERVICESELECTOR_H
#define SERVICESELECTOR_H

#include <QWidget>
#include <EntityGroupTableWnd.h>


class ServiceSelector : public EntityGroupTableWnd
{
    Q_OBJECT

public:
    explicit ServiceSelector(MainWnd* pMainWnd, QString sTitle);
    ~ServiceSelector();

    virtual void OnCreate();
    void Populate(bool bAceptChanges = true);
protected slots:
    virtual void OnAddNew(std::shared_ptr<Entity> pGroup) override;
    virtual void OnEdit(std::shared_ptr<Entity> pEntity) override;
    virtual void OnCopy(std::shared_ptr<Entity> pEntity) override;

    void OnServiceAdded(bool bEdit);
private:
    GroupingParameters o_Sel;
    bool b_Populated;
};

#endif // SERVICESELECTOR_H
