#include "ServiceGroup.h"
#include <crmglobaldefs.h>
#include <CRMDBFields.h>
ServiceGroup::ServiceGroup()
{
    s_PersistatName = TBLNM_CRM_SERVICEGROUP;
    s_DefName = ENTITY_SERVICE_GROUP;
    s_Key = FLD_SG_ID;
    SetKeyAutoIncrement(true);
}

QString ServiceGroup::GetServiceGroupName()
{
    return GetData(FDNM_CRM_SERVICEGROUP_NAME).toString();
}

QString ServiceGroup::GetServiceGroupID()
{
    return GetData(FDNM_CRM_SERVICEGROUP_SERVICEGROUPID).toString();
}

