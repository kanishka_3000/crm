/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "AddServicesWnd.h"


AddServicesWnd::AddServicesWnd(MainWnd *pMainWnd, QString sTitle) :
  GenericWnd(pMainWnd, sTitle)
{
    setupUi(this);
}

void AddServicesWnd::SetServiceGroup(std::shared_ptr<ServiceGroup> pServiceGroup)
{
    p_Ctrl->SetServiceGroup(pServiceGroup);
}

void AddServicesWnd::SetCallback(QWidget *pWnd)
{
    connect(p_Ctrl, SIGNAL(NotifyItemSaved(bool)), pWnd, SLOT(OnServiceAdded(bool)));
}

AddServicesWnd::~AddServicesWnd()
{

}

void AddServicesWnd::OnEdit(std::shared_ptr<Service> pService)
{
    p_Ctrl->PopulateWithEntity(pService);
}

void AddServicesWnd::OnCopy(std::shared_ptr<Service> pService)
{
     p_Ctrl->OpenWithService(pService);

}
