/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ManageServicesGroupWnd.h"
#include <AddServiceGroupCtrl.h>
#include <crmglobaldefs.h>
#include <ManageServicesGroupCtrl.h>
#include <QDebug>
ManageServicesGroupWnd::ManageServicesGroupWnd(MainWnd* pParent, QString sTitle) :
    GenericWnd(pParent, sTitle)
{
    setupUi(this);
    p_EditCtrl = NULL;
    p_Query->SetTableName(ENTITY_SERVICE_GROUP);
    p_Query->SetKeyName(FLD_SG_ID);
}

void ManageServicesGroupWnd::OnCreate()
{
    connect(p_Query,SIGNAL(NotifyNewRequest()),this, SLOT(OnAddService()));
    connect(p_Query,SIGNAL(NotifyEditRequest(QString)),this,SLOT(OnEditRequest(QString)));
     ReSize(400,400);
}

void ManageServicesGroupWnd::OnChildRemoveRequest(GenericCtrl *pChild)
{
    if(pChild == p_EditCtrl)
    {
        qDebug() << "Child removed";
        //delete p_EditCtrl;
        p_EditCtrl = NULL;
    }
}

ManageServicesGroupWnd::~ManageServicesGroupWnd()
{

}

void ManageServicesGroupWnd::OnAddService()
{

    tab_Ctrls->setCurrentIndex(1);
}

void ManageServicesGroupWnd::OnEditRequest(QString sKeyValue)
{
//    if(p_EditCtrl)
//    {
//        delete p_EditCtrl;
//    }
    std::shared_ptr<Entity> pEntity = Entity::FindInstance(sKeyValue,ENTITY_SERVICE_GROUP);
    p_EditCtrl = new AddServiceGroupCtrl(this);
    OnNewChild(p_EditCtrl);
    int iTab = tab_Ctrls->addTab(p_EditCtrl,tr("Edit Group"));
    tab_Ctrls->setCurrentIndex(iTab);
    p_EditCtrl->PopulateWithEntity(pEntity);

}


