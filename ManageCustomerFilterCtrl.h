/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef MANAGECUSTOMERFILTERCTRL_H
#define MANAGECUSTOMERFILTERCTRL_H

#include <QWidget>
#include "ui_ManageCustomerFilterCtrl.h"
#include <GenericCtrl.h>
#include <GenericFilterCtrl.h>
#include <FieldCompleter.h>
class ManageCustomerFilterCtrl : public GenericFilterCtrl,public Ui::ManageCustomerFilterCtrl
{
    Q_OBJECT

public:
    explicit ManageCustomerFilterCtrl(QWidget *parent = 0);
    ~ManageCustomerFilterCtrl();
    virtual void OnCreateCtrl();
private:
      FieldCompleter* p_Clm;
};

#endif // MANAGECUSTOMERFILTERCTRL_H
