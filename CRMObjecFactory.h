/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef CRMOBJECFACTORY_H
#define CRMOBJECFACTORY_H
#include <ObjectFactory.h>

class CRMObjecFactory:public ObjectFactory
{
public:
    CRMObjecFactory();

      virtual MainWnd* CreateMainWnd();
    virtual EntityFactory* CreateEntityFactory();

    // ObjectFactory interface
public:
    virtual DataSetupManager *GetSetupManager();
};

#endif // CRMOBJECFACTORY_H
