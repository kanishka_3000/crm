/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "Service.h"
#include <crmglobaldefs.h>
#include <CRMDBFields.h>
#include <Util.h>
#include <ServiceGroup.h>
Service::Service()
{
    s_PersistatName = TBLNM_CRM_SERVICES;
    s_DefName = ENTITY_SERVICE;
    s_Key = FLD_SERVICE_ID;
    p_ServiceGroup = nullptr;
}

int Service::GetServiceID()
{
    return map_Data.value(FDNM_CRM_SERVICES_SERVICEID).toInt();
}

QString Service::GetServiceName()
{
    return map_Data.value(FDNM_CRM_SERVICES_SERVICENAME).toString();
}

QString Service::GetServiceDescription()
{
    return map_Data.value(FDNM_CRM_SERVICES_SERVICEDESCRIPTION).toString();
}

double Service::GetValue()
{
    return GetData(FDNM_CRM_SERVICES_VALUE).toDouble();
}

int Service::GetStatus()
{
    return GetData(FDNM_CRM_SERVICES_STATE).toInt();
}

QVariant Service::GetData(QString sFieldName)
{
    if(sFieldName == FDNM_CRM_SERVICES_VALUE)
    {
        return Util::DoubleToString(Entity::GetData(sFieldName).toDouble());
    }
    return Entity::GetData(sFieldName);
}

std::shared_ptr<ServiceGroup> Service::GetServiceGroup()
{
    if(p_ServiceGroup == nullptr)
    {
        p_ServiceGroup = std::static_pointer_cast<ServiceGroup>(Entity::FindInstance(GetData(FDNM_CRM_SERVICES_GROUPID).toString(), ENTITY_SERVICE_GROUP));
    }

    return p_ServiceGroup;

}

