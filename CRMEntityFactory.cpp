#include "CRMEntityFactory.h"
#include <Customer.h>
#include <CRMDBFields.h>
#include <crmglobaldefs.h>
#include <ServiceGroup.h>
#include <Service.h>
#include <Order.h>
#include <Task.h>
#include <Payment.h>
CRMEntityFactory::CRMEntityFactory()
{
    map_EntityNames.insert(ENTITY_CUSTOMER,FLD_PHONE);
    map_EntityNames.insert(ENTITY_SERVICE_GROUP, FLD_SG_ID);
    map_EntityNames.insert(ENTITY_SERVICE, FLD_SERVICE_ID);
    map_EntityNames.insert(TBLNM_CRM_CLIENTORDER,FDNM_CRM_CLIENTORDER_ORDERID);
    map_EntityNames.insert(TBLNM_CRM_TASK,FDNM_CRM_TASK_TASKID);
    map_EntityNames.insert(TBLNM_CRM_PAYMENT, FDNM_CRM_PAYMENT_PAYMENTID);
}

void CRMEntityFactory::RegisterEntities()
{
    EntityFactory::RegisterEntities();
    SetHandler<Customer>(ENTITY_CUSTOMER);
    SetHandler<ServiceGroup>(ENTITY_SERVICE_GROUP);
    SetHandler<Service>(ENTITY_SERVICE);
    SetHandler<Order>(TBLNM_CRM_CLIENTORDER);
    SetHandler<Task>(TBLNM_CRM_TASK);
    SetHandler<Payment>(TBLNM_CRM_PAYMENT);
}

