/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef PRODUCTIONGRAPHFILTERCTRL_H
#define PRODUCTIONGRAPHFILTERCTRL_H

#include "ui_ProductionGraphFilterCtrl.h"
#include <GenericFilterCtrl.h>
#define FILTER_PRODCTION_START_DATE "startdate"
#define FILTER_PRODUCTION_END_DATE "enddate"
#define FILTER_PRODUCT   "product"
#define FILTER_GRAPH  "graph_type"


#define PRODUCT_COUNT_GRAPH "Product Count"
#define PRODUCTS_PER_TIME "Product Per Time"
#define PAYMENT_GRAPH "Payments Per Time"
class ProductionGraphFilterCtrl : public GenericFilterCtrl, private Ui::ProductionGraphFilterCtrl
{
    Q_OBJECT

public:
    explicit ProductionGraphFilterCtrl(QWidget *parent = 0);

    // GenericCtrl interface
public:
    virtual void OnCreateCtrl();

    void FillServices();

    // GenericFilterCtrl interface
public slots:
    virtual void OnFilter();
};

#endif // PRODUCTIONGRAPHFILTERCTRL_H
