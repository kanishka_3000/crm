/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef CRMPREFERENCEWND_H
#define CRMPREFERENCEWND_H

#include <QObject>
#include <PrefereneWnd.h>
class CRMPreferenceWnd : public PrefereneWnd
{
public:
    CRMPreferenceWnd(MainWnd *parent,QString sTitle);

    // PrefereneWnd interface
protected:
    virtual void AddCustomTabs() override;
};

#endif // CRMPREFERENCEWND_H
