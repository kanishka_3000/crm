/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "OrderQueryWnd.h"
#include <CRMDBFields.h>
#include <crmglobaldefs.h>
#include <Order.h>
#include <OrderWnd.h>
OrderQueryWnd::OrderQueryWnd(MainWnd* pMainWnd,QString sTitle) :
GenericWnd(pMainWnd, sTitle)
{
    setupUi(this);
    p_QueryCtrl->SetTableName(TBLNM_CRM_CLIENTORDER);
    p_QueryCtrl->SetKeyName(FDNM_CRM_CLIENTORDER_ORDERID);
    p_QueryCtrl->SetCommandHanlder(this);
    p_QueryCtrl->SetConfigName("Order Query");
}

void OrderQueryWnd::SetFilter(QMap<QString, QString> mapData)
{
    p_Filter->SetFilter(mapData);
    p_Filter->OnFilter();
}

void OrderQueryWnd::OnCreate()
{

    connect(p_Filter,SIGNAL(NotifyFilter(QList<FilterCrieteria>)),p_QueryCtrl, SLOT(OnFilter(QList<FilterCrieteria>)));
    connect(p_QueryCtrl,SIGNAL(NotifyNewRequest()),this, SLOT(OnNewOrder()));
    connect(p_QueryCtrl,SIGNAL(NotifyEditRequest(QString)),this,SLOT(OnEditRequest(QString)));
    ReSize(600,300);
}



OrderQueryWnd::~OrderQueryWnd()
{

}

void OrderQueryWnd::OnNewOrder()
{
    OrderWnd* pOrderWnd = static_cast<OrderWnd*>(p_Application->CreateWnd(WND_ORDER));
    pOrderWnd->OpenIndependant();
}

void OrderQueryWnd::OnEditRequest(QString sKey)
{
    std::shared_ptr<Order> pOrder = std::static_pointer_cast<Order>(Entity::FindInstance(sKey, TBLNM_CRM_CLIENTORDER));
    GenericWnd* pW = p_Application->CreateWnd(WND_ORDER);
    OrderWnd* pOrderQuery = static_cast<OrderWnd*>(pW);
    pOrderQuery->InitliazeWithOrder(pOrder);

}

void OrderQueryWnd::UpdateEditCommandName(QString &rEditButtnName)
{
    rEditButtnName = tr("Open Order");
}

void OrderQueryWnd::UpdateNewCommandName(QString &rNewCommandName)
{
    rNewCommandName = tr("Add Order");
}


