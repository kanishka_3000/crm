/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "OrderCtrl.h"
#include <crmglobaldefs.h>
#include <CRMDBFields.h>
#include <GenericWnd.h>
#include <ServiceSelector.h>
#include <Service.h>
#include <Task.h>
#include <SpinBoxDeligate.h>
#include <QMessageBox>
#include <PaymentEntryWnd.h>
#include <FieldCompleter.h>
#include <QPropertyAnimation>
#include <QGraphicsOpacityEffect>
#include <Payment.h>
#include <QDebug>
#include <LabelAnimator.h>
#include <Util.h>
#include <feature/DirectoryManager.h>
OrderCtrl::OrderCtrl(QWidget *parent) :
EntityManipulaterCtrl(parent)

{
    setupUi(this);
    p_Order = nullptr;
    txt_ContactNo->setProperty(FIELD,FDNM_CRM_CLIENTORDER_CUSTOMERID);
    dt_DeliveryDay->setProperty(FIELD, FDNM_CRM_CLIENTORDER_DELIVERYDATE);
    cmb_Responsiblity->setProperty(FIELD, FDNM_CRM_CLIENTORDER_ASIGNEE);
    txt_OrderTakenBy->setProperty(FIELD,FDNM_CRM_CLIENTORDER_TAKENBY);
    txt_Comment->setProperty(FIELD,FDNM_CRM_CLIENTORDER_COMMENT);
    cmb_OrderStatus->setProperty(FIELD, FDNM_CRM_CLIENTORDER_STATE);
    cmb_OrderStatus->setProperty(ENUM_NAME, E_ORDER_STATE);

    txt_Comment->setTabChangesFocus(true);
    SetEntityType(TBLNM_CRM_CLIENTORDER);
    p_ServiceDataHandler = new ServiceDataHandler();
    p_ServiceView->setItemDelegateForColumn(VALUE_FIELD,new SpinBoxDeligate(this));
    SetClearFieldsWhenSave(false);
    p_PaymentQuery->SetAutoSelect(false);
    b_OrderSaved = false;
    d_OrderValue = 0;
    d_PayedAmount = 0;
    d_Payable = 0;
    p_PaymentQuery->SetKeyName(FDNM_CRM_PAYMENT_PAYMENTID);
    p_PaymentQuery->SetCommandHanlder(this);
    p_PaymentQuery->SetEnableReload(false);

}

OrderCtrl::~OrderCtrl()
{
    delete p_ServiceDataHandler;
    delete p_Clm;
}

void OrderCtrl::InitializeReportManager()
{
    p_ReportManager->RegisterConfigField(ORG_NAME);
    p_ReportManager->RegisterConfigField(ORG_ADDRESS);
    p_ReportManager->RegisterConfigField(ORG_CONTACT);
    p_ReportManager->RegisterConfigField(ORG_EMAIL);

    p_ReportManager->RegisterExternalField("customerid");
    p_ReportManager->RegisterExternalField("name");
    p_ReportManager->RegisterExternalField("address");
    p_ReportManager->RegisterExternalField("invoicedate");
    p_ReportManager->RegisterExternalField("orderid");
    p_ReportManager->RegisterExternalField("total");
    p_ReportManager->RegisterExternalField("balance");
    p_ReportManager->RegisterExternalField("authorize");
    p_ReportManager->RegisterExternalField("paid");
    p_ReportManager->RegisterModel(p_ServiceModel);
    p_ReportManager->SetExternlaDataProvider(this);
}

void OrderCtrl::UpdateEditCommandName(QString &rEditButtnName)
{
    rEditButtnName = tr("Change Payment");
}

void OrderCtrl::UpdateNewCommandName(QString &rNewCommandName)
{
    rNewCommandName = tr("New Payment");
}

void OrderCtrl::OnCreateCtrl()
{
    SetCurrentDateToAllFields();
    oConfig = p_Parent->GetWindowTableConfiguration();
    p_ServiceModel = new EntitiyReporableModel(this, &oConfig,&o_Resolver);
    p_ServiceModel->SetDataCalback(p_ServiceDataHandler);
    p_ServiceView->setModel(p_ServiceModel);
    p_UserTable = new QSqlTableModel(this, *Connection::GetInstance()->GetPrimaryDb());
    p_UserTable->setTable(TBLNM_APP_LOGIN);
    p_UserTable->select();
    cmb_Responsiblity->setModel(p_UserTable);
    QString sUserName = p_Parent->GetApplication()->GetUserName();
    txt_OrderTakenBy->setText(sUserName);
    p_ReportManager = new reporting::ReportManager("Order",p_Parent->GetApplication()->GetConfig());


    FillComboWithEnum(cmb_OrderStatus, E_ORDER_STATE);
    cmb_OrderStatus->setCurrentIndex(2);

    p_Clm = new FieldCompleter(p_Parent->GetApplication(),TBLNM_CRM_CUSTOMER);
    connect(btn_AddService,SIGNAL(clicked(bool)),this ,SLOT(OnAddServices()));
    connect(btn_Remove, SIGNAL(clicked(bool)),this,SLOT(OnRemove()));
    connect(btn_Done, SIGNAL(clicked(bool)),this,SLOT(OnSave()));
    connect(p_ServiceModel, SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),this,
            SLOT(OnModelChanged(QModelIndex,QModelIndex)));
    connect(p_SerSelector, SIGNAL(NotifiyDataSelected(QList<std::shared_ptr<Entity> >,bool)),this,
            SLOT(OnServicesSelected(QList<std::shared_ptr<Entity> >,bool)));
    connect(btn_Print, SIGNAL(clicked(bool)),this, SLOT(OnPrint()));
    connect(btn_Delivered, SIGNAL(clicked(bool)), this, SLOT(OnDelivered()));
    connect(p_PaymentQuery, SIGNAL(NotifyNewRequest()), this, SLOT(OnMakePayment()));
    connect(p_PaymentQuery, SIGNAL(NotifyEditRequest(QString)), this, SLOT(OnPaymentEdit(QString)));
    InitializeReportManager();
    InitializePaymentQuery();
}

void OrderCtrl::InitializePaymentQuery()
{
    p_Logger->WriteLog("Initializing Payment Query");
    p_PaymentQuery->SetConfigName("Paymentquery");
    p_PaymentQuery->SetTableName(TBLNM_CRM_PAYMENT);
    //p_PaymentQuery->Initialize(false);
}

void OrderCtrl::CreateDirectories()
{
    if(!p_Order)
        return;


    if(!p_Parent->GetApplication()->GetConfigData(PREF_CNT_ENABLE).toBool())
        return;

    QString sRoot = p_Parent->GetApplication()->GetConfigData(PREF_ROOT).toString();
    if( sRoot == "")
    {
        sRoot = QDir::homePath();
    }
    std::shared_ptr<Customer> pCustomer = p_Order->GetCustomer();
    QString sBaseDir = pCustomer->GetCustomerName();

    sBaseDir = sBaseDir.toUpper();
    sBaseDir.replace(" ","_");
    sBaseDir += QString("_") + p_Order->GetOrderID();

    QStringList oList;
    oList.push_back(p_Parent->GetApplication()->GetConfigData(PREF_SOURCE).toString());
    oList.push_back(p_Parent->GetApplication()->GetConfigData(PREF_PROCESS).toString());
    oList.push_back(p_Parent->GetApplication()->GetConfigData(PREF_COMPLTE).toString());
    feature::DirectoryManager oDirManager(sRoot, sBaseDir, oList,this);
    oDirManager.Create();

}

void OrderCtrl::AdjustPaymentQuery()
{
    FilterCrieteria oCritera(FDNM_CRM_PAYMENT_ORDERID,p_Order->GetOrderID(),FILTER_TYPE_EXACT);
    QList<FilterCrieteria> lstFilterList;
    lstFilterList.push_back(oCritera);
    p_PaymentQuery->OnFilter(lstFilterList);
    p_PaymentQuery->Reload();

    double dTotalAmount = 0.0;
    QList<std::shared_ptr<Entity>> lstPayments = Entity::FindInstances(FDNM_CRM_PAYMENT_ORDERID,p_Order->GetOrderID(),TBLNM_CRM_PAYMENT);
    foreach(std::shared_ptr<Entity> pEntity, lstPayments)
    {
        std::shared_ptr<Payment> pPayment = std::static_pointer_cast<Payment>(pEntity);
        dTotalAmount += pPayment->GetAmount();
    }
    d_PayedAmount = dTotalAmount;
    txt_Payments->setText(QVariant(dTotalAmount).toString());
    CalculateTotalPayable();
}

QVariant OrderCtrl::ProvideReportData(QString sFieldName)
{
    if(!p_Order)
        return "";

    if(sFieldName == FDNM_CRM_CUSTOMER_NAME)
    {
        return p_Order->GetCustomer()->GetCustomerName();
    }
    if(sFieldName == FDNM_CRM_CLIENTORDER_CUSTOMERID)
    {
         return p_Order->GetCustomerID();
    }
    if(sFieldName == FDNM_CRM_CUSTOMER_ADDRESS)
    {
        return p_Order->GetCustomer()->GetAddress();
    }
    if(sFieldName == "invoicedate")
    {
        return p_Order->GetDeliveryDate();
    }
    if(sFieldName == FDNM_CRM_CLIENTORDER_ORDERID)
    {
        return p_Order->GetOrderID();
    }
    if(sFieldName == "total")
    {
        return Util::DoubleToString(d_OrderValue);
    }
    if(sFieldName == "balance")
    {
        return Util::DoubleToString((d_Payable));
    }
    if(sFieldName == "paid")
    {
        return Util::DoubleToString(d_PayedAmount);
    }
    if(sFieldName == "authorize")
    {
        return p_Parent->GetApplication()->GetUserName();
    }
    return "";
}

void OrderCtrl::InitializeWithCustomer(std::shared_ptr<Customer> pCustomer)
{
    txt_CustomerName->setText(pCustomer->GetCustomerName());
    txt_ContactNo->setText(QVariant(pCustomer->GetContactNo()).toString());
    txt_CustomerName->setReadOnly(true);
}

void OrderCtrl::EditOrder(std::shared_ptr<Order> pOrder)
{
    p_ServiceModel->Clear();
    b_EditMode = true;
    p_Order = pOrder;
    std::shared_ptr<Customer> pCustomer = std::static_pointer_cast<Customer>(Entity::FindInstance(pOrder->GetCustomerID(),TBLNM_CRM_CUSTOMER));
    InitializeWithCustomer(pCustomer);
    PopulateWithEntity(pOrder);

    QList<std::shared_ptr<Entity>> lstTasks = Entity::FindInstances(FDNM_CRM_TASK_ORDERID,pOrder->GetOrderID(),TBLNM_CRM_TASK);

    foreach(std::shared_ptr<Entity> pEty, lstTasks)
    {
        std::shared_ptr<Task> pTask = std::static_pointer_cast<Task>(pEty);
        pTask->ResolveServices();
        pTask->SetUpdate(true);
        pTask->SetTotalValue();
        set_OriginalTasks.insert(pTask);
        p_ServiceModel->InsertRow(QVariant(pTask->GetService()->GetServiceID()).toString(),pTask);
    }
    SetTotalOrderValue();
    AdjustPaymentQuery();
}

void OrderCtrl::OpenIndependant()
{
    txt_CustomerName->setReadOnly(false);

    p_Clm->SetSource(txt_CustomerName, FDNM_CRM_CUSTOMER_NAME);
    p_Clm->SetDestination(txt_ContactNo, FDNM_CRM_CUSTOMER_PHONE);

}

void OrderCtrl::SetTotalOrderValue()
{
    double dTotalValue = 0.0;
    int iSize = p_ServiceModel->rowCount();
    for(int i = 0 ;i < iSize; i++)
    {
        std::shared_ptr<void> pAbstractTask = p_ServiceModel->GetData(i);
        std::shared_ptr<Task> pTask = std::static_pointer_cast<Task>(pAbstractTask);
        dTotalValue += pTask->GetTotalValue();
    }
    QString sBill = QString("Rs: %1").arg(dTotalValue, 0, 'f', 2);
    txt_TotalBill->setText(sBill);
    txt_TotalBill->setToolTip(sBill);
    d_OrderValue = dTotalValue;
    CalculateTotalPayable();
}

void OrderCtrl::CalculateTotalPayable()
{
    d_Payable = d_OrderValue - d_PayedAmount;
    QString sValue;
    sValue.sprintf("%.2f",d_Payable);
    txt_Payable->setText(sValue);
    txt_Payable->setToolTip(sValue);
}

bool OrderCtrl::CustomValidations(QString &sError)
{
    if(txt_ContactNo->text().isEmpty())
    {
        sError = tr("Please select a valid customer first");
        return false;
    }
    int iSize = p_ServiceModel->rowCount();
    if(iSize < 1)
    {
        sError = tr("Select at least one service");
        return false;
    }
    return true;
}

void OrderCtrl::OnAddServices()
{
    GenericWnd* pGenericWnd = p_Parent->GetApplication()->CreateWnd(WND_SERVICE_SELECTOR);
    ServiceSelector* pServiceSelecWnd  = static_cast<ServiceSelector*>(pGenericWnd);
    connect(pServiceSelecWnd,SIGNAL(NotifiyDataSelected(QList<std::shared_ptr<Entity> >,bool)),this,
            SLOT(OnServicesSelected(QList<std::shared_ptr<Entity> >,bool)));

    pServiceSelecWnd->Populate(false);


}

void OrderCtrl::OnServicesSelected(QList<std::shared_ptr<Entity> > pEntities, bool bSelectNClose)
{
    (void)bSelectNClose;
    foreach(std::shared_ptr<Entity> pEntity, pEntities)
    {
        std::shared_ptr<Service> pService = std::static_pointer_cast<Service>(pEntity);
        std::shared_ptr<Task> pTask = std::static_pointer_cast<Task>(p_Parent->GetApplication()->GetEntityFactory()->CreateEntity(TBLNM_CRM_TASK));
        if(b_EditMode)
        {
            pTask->SetOrderID(p_Order->GetOrderID().toInt());
        }
        pTask->SetService(pService);
        pTask->SetCount(1);
        pTask->SetTotalValue();
        p_ServiceModel->InsertRow(QVariant(pService->GetServiceID()).toString(),pTask);
    }
    SetTotalOrderValue();
}

void OrderCtrl::OnRemove()
{
    if(b_EditMode)
    {
        QMessageBox::information(this, tr("Invalid"), tr("It is not possible to "
                                                         "remove services, during order "
                                                         "amend<br/>Try setting item count to 0"));
        return;
    }
    QMessageBox::StandardButton eButton =QMessageBox::question(this, tr("Remove Selected"), tr("Are you sure you "
                                                                                               "want to remove selected services ?"));
    if(eButton != QMessageBox::StandardButton::Yes)
    {
        return;
    }
    QSet<QString> setCheckedItems;
    p_ServiceModel->GetCheckedKeys(setCheckedItems);
    foreach(QString sKey, setCheckedItems)
    {
        p_ServiceModel->RemoveRow(sKey);
    }
    SetTotalOrderValue();
}

void OrderCtrl::OnSave()
{
    if(b_OrderSaved)
    {
        QMessageBox::information(this, tr("Saved"), tr("Order is already saved"));
        return;
    }

    //Save the Order first
    OrderCtrlSaveDislpayer oDislpayer(lbl_Notification, this);
    if(!EntityManipulaterCtrl::OnSave(&oDislpayer))
    {
        return;
    }

    p_Order = std::static_pointer_cast<Order>(p_Entity);
    int iOrderID = p_Order->GetOrderID().toInt();

    int iSize = p_ServiceModel->rowCount();
    for(int i = 0 ;i < iSize; i++)
    {

        std::shared_ptr<void> pAbstractTask = p_ServiceModel->GetData(i);
        std::shared_ptr<Task> pTask = std::static_pointer_cast<Task>(pAbstractTask);


        if(!b_EditMode)
        {
            pTask->SetOrderID(iOrderID);
        }
        Application::GetApplication()->GetLogger()->WriteLog("Saving Task %s", qPrintable(pTask->GetDebugString()));
        pTask->Save();
    }
    b_OrderSaved = true;
    if((!b_EditMode) )
    {
        CreateDirectories();
    }

}

void OrderCtrl::OnModelChanged(QModelIndex oBegin, QModelIndex oEnd)
{
    (void)oEnd;
    if(oBegin.column() == VALUE_FIELD)
    {
        SetTotalOrderValue();
    }
}

void OrderCtrl::OnMakePayment()
{
    if(!p_Order)
    {
        QMessageBox::information(this, tr("Oder does not exist"), tr("Save the order first"));
        return;
    }
    GenericWnd* pW = p_Parent->GetApplication()->CreateWnd(WND_PAYMENT_ENTRY);
    PaymentEntryWnd* pPayementEndWnd = static_cast<PaymentEntryWnd*>(pW);
    pPayementEndWnd->InitalizeWithOrder(p_Order);
    pPayementEndWnd->SetPaymentNotifier(this);

}

void OrderCtrl::OnPayment()
{
    p_PaymentQuery->Reload();
    AdjustPaymentQuery();
}

void OrderCtrl::OnPrint()
{
    p_ReportManager->Initialize();
}

void OrderCtrl::OnDelivered()
{
    QMessageBox::StandardButton eButton =QMessageBox::question(this, tr("Order Delivered"), tr("Are you sure you "
                                                                                               "this order is delivered ?"));
    if(eButton != QMessageBox::StandardButton::Yes)
    {
        return;
    }
    SelectEnumValue(cmb_OrderStatus, 1);
    btn_Delivered->setEnabled(false);
}

void OrderCtrl::OnPaymentEdit(QString sPaymentID)
{
    std::shared_ptr<Payment> pPayment = Entity::FindInstaceEx<Payment>(sPaymentID, TBLNM_CRM_PAYMENT);
    PaymentEntryWnd* pPaymentWnd = p_Parent->GetApplication()->CreateWndEx<PaymentEntryWnd>(WND_PAYMENT_ENTRY);
    pPaymentWnd->PopulateWithPayment(pPayment);
}

OrderCtrl::OrderCtrlSaveDislpayer::OrderCtrlSaveDislpayer(QLabel *pOutput, QWidget *parent)
{
    p_Label = pOutput;
    p_Parent = parent;
}

void OrderCtrl::OrderCtrlSaveDislpayer::EntitySaved(bool bEdit)
{
    QString sMsg;
    if(!bEdit)
    {
        sMsg = QCoreApplication::translate("OrderCtrl::OrderCtrlSaveDislpayer", "Your order has been saved, <br/> "
                "you may want to proceed to payments tab");
    }
    else
    {
        sMsg = QCoreApplication::translate("OrderCtrl::OrderCtrlSaveDislpayer", "Your order was successfully edited");
    }
    QMessageBox::information(p_Parent, QCoreApplication::translate("OrderCtrl::OrderCtrlSaveDislpayer",
                                                                   "Suceess"), sMsg);
}

void OrderCtrl::OrderCtrlSaveDislpayer::CustomizeExistMsg(QString &sMsg)
{
    sMsg = QCoreApplication::translate("OrderCtrl::OrderCtrlSaveDislpayer", "Order with same contact information exists <br/> Please query using the contact information");
}


void OrderCtrl::OrderTaskDependacyResolver::Resolve(std::shared_ptr<EntityReportData> pData)
{
    std::shared_ptr<Entity> pAbsEntity = pData->GetPrimaryEntity();
    std::shared_ptr<Task> pTask = std::dynamic_pointer_cast<Task>(pAbsEntity);

    pData->AddEntity(pTask->GetService());
}
