/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef CUSTOMERREGISTERWND_H
#define CUSTOMERREGISTERWND_H

#include <QWidget>
#include <GenericWnd.h>
#include <ui_CustomerRegisterWnd.h>
class CustomerRegisterWnd : public GenericWnd,public Ui::CustomerRegisterWnd
{
    Q_OBJECT

public:
    explicit CustomerRegisterWnd(MainWnd *parent,QString sTitle);
    ~CustomerRegisterWnd();
    virtual void OpenAsEdit(std::shared_ptr<Entity> pEntity);
private:

};

#endif // CUSTOMERREGISTERWND_H
