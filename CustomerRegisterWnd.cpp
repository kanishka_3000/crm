#include "CustomerRegisterWnd.h"
#include "ui_CustomerRegisterWnd.h"

CustomerRegisterWnd::CustomerRegisterWnd(MainWnd *parent, QString sTitle) :
   GenericWnd(parent,sTitle)
{
    setupUi(this);
}

CustomerRegisterWnd::~CustomerRegisterWnd()
{

}

void CustomerRegisterWnd::OpenAsEdit(std::shared_ptr<Entity> pEntity)
{
    p_Ctrl->PopulateWithEntity(pEntity);
}
