/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "AddServicesCtrl.h"
#include <crmglobaldefs.h>
#include <Connection.h>
#include <GenericWnd.h>
#include <CRMDBFields.h>
#include <QMessageBox>
AddServicesCtrl::AddServicesCtrl(QWidget *parent) :
    EntityManipulaterCtrl(parent)
{
    setupUi(this);
    SetEntityType(ENTITY_SERVICE);
    connect(btn_Save,SIGNAL(clicked(bool)),this,SLOT(OnSave()));
    txt_GroupID->setVisible(false);
}

AddServicesCtrl::~AddServicesCtrl()
{

}

void AddServicesCtrl::OnCreateCtrl()
{


    FillComboWithEnum(cmb_State,E_STATE);

}

void AddServicesCtrl::SetServiceGroup(std::shared_ptr<ServiceGroup> pServiceGroup)
{
    txt_GroupName->setText(pServiceGroup->GetServiceGroupName());
    txt_GroupID->setText(pServiceGroup->GetServiceGroupID());
}

void AddServicesCtrl::OpenWithService(std::shared_ptr<Service> pService)
{
    if(pService == nullptr)
    {
        QMessageBox::information(this, tr("Invalid Selection"), tr("Please select a service"));
        return;
    }
    std::shared_ptr<Service> pScopy = std::static_pointer_cast<Service>(p_Parent->GetApplication()->GetEntityFactory()->CreateEntity(ENTITY_SERVICE));
    (*pScopy) = *(pService.get());
    pScopy->ResetPrimaryKey();
    PopulateWithEntity(pScopy, false);
    //AdjustFieldsAsCopy();
    txt_GroupName->setText(pService->GetServiceGroup()->GetServiceGroupName());
    spin_Value->setReadOnly(false);
    spin_Value->setToolTip(tr(""));
    int iServiceID = pService->GetServiceID();
    iServiceID+=SERVICE_OFFSET;
    QString sCopyService = QString("%1-R%2").arg(pService->GetServiceName()).arg(iServiceID);
    txt_ServiceName->setText(sCopyService);

    QString sServiceDescription = QString("%1 - Revision of Service %2").arg(GetWidgetText(txt_Description)).arg(pService->GetServiceID());
    SetWidgetText(txt_Description, sServiceDescription);

}

void AddServicesCtrl::PopulateWithEntity(std::shared_ptr<Entity> pEntity, bool bEditMode)
{
    EntityManipulaterCtrl::PopulateWithEntity(pEntity, bEditMode);
    spin_Value->setReadOnly(true);
    spin_Value->setToolTip(tr("Not allowed to edit value, Please copy service for amendments"));
}

bool AddServicesCtrl::CustomValidations(QString &sError)
{
    if( (!b_EditMode) && (!p_Entity->IsFieldUnique(FDNM_CRM_SERVICES_SERVICENAME)))
    {
        sError = tr("Service name is not unique");
        return false;
    }
    return true;
}
