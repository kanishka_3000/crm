/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef TASK_H
#define TASK_H
#include <Entity.h>
#include <CRMDBFields.h>
#include <Service.h>
class Task: public Entity
{
public:
    Task();
     void SetService(std::shared_ptr<Service> pService);
     std::shared_ptr<Service>  GetService();

     int GetCount();
     void SetCount(int iCount);
     QString GetDeliveryDate();
     void SetDeliveryDate(QString sDate);
     QString GetAssignee();
     void SetAssignee(QString sAssignee);

     void SetOrderID(int iOrderId);

     void ResolveServices();
     void SetTotalValue();
     double GetTotalValue(){return d_TotalValue;}

     QVariant GetData(QString sFieldName);
     std::shared_ptr<Service> p_Service;
private:
       double d_TotalValue;
};

#endif // TASK_H
