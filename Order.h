/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ORDER_H
#define ORDER_H
#include <Entity.h>
#include <Customer.h>
#include <set>
#include <Task.h>
#include <Payment.h>
class Order:public Entity
{
public:
    Order();
    QString GetCustomerID();
    QString GetOrderID();
    QString GetDeliveryDate();

    std::shared_ptr<Customer> GetCustomer();
    QList<std::shared_ptr<Entity> > lst_Task;
    QList<std::shared_ptr<Entity>> lst_Payment;
private:
    std::shared_ptr<Customer> p_Customer;

    // Entity interface
public:
    virtual void ResolveReferences();
};

#endif // ORDER_H
