/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef CMSPREFERENCETAB_H
#define CMSPREFERENCETAB_H

#include <ui_CMSPreferenceTab.h>
#include <GenericCtrl.h>
#include <BasePreferenceTab.h>
#include <QFileDialog>
class CMSPreferenceTab : public BasePreferenceTab, private Ui::CMSPreferenceTab
{
    Q_OBJECT

public:
    explicit CMSPreferenceTab(QWidget *parent = 0);
public slots:
    void OnOpenRoot();

    // GenericCtrl interface
public:
    virtual void OnCreateCtrl();
};

#endif // CMSPREFERENCETAB_H
