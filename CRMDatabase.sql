BEGIN TRANSACTION;
CREATE TABLE `Task` (
	`taskid`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`orderid`	INTEGER,
	`serviceid`	INTEGER,
	`count`	INTEGER,
	`deliverydate`	TEXT,
	`assignee`	TEXT
);
INSERT INTO `Task` VALUES(1,6,7,'','','');
INSERT INTO `Task` VALUES(2,1,8,'','','');
INSERT INTO `Task` VALUES(3,7,7,2,'','');
INSERT INTO `Task` VALUES(4,3,8,3,'','');
INSERT INTO `Task` VALUES(5,8,4,'','','');
INSERT INTO `Task` VALUES(6,5,6,'','','');
INSERT INTO `Task` VALUES(7,9,1,7,'','');
INSERT INTO `Task` VALUES(8,7,7,4,'','');
INSERT INTO `Task` VALUES(9,8,3,4,'','');
INSERT INTO `Task` VALUES(10,9,4,10,'','');
CREATE TABLE `Services` (
	`serviceid`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`servicename`	TEXT,
	`servicedescription`	TEXT,
	`state`	INTEGER,
	`value`	INTEGER,
	`groupid`	INTEGER
);
INSERT INTO `Services` VALUES(1,'Test serice','teser			',0,'800.67',6);
INSERT INTO `Services` VALUES(2,'test service 7','teser			',0,'815.45',7);
INSERT INTO `Services` VALUES(3,'testing 8','teser			',0,'1115.45',8);
INSERT INTO `Services` VALUES(4,'ata','teser			',0,'1315.45',9);
INSERT INTO `Services` VALUES(5,'kan service','kan service',0,300,7);
INSERT INTO `Services` VALUES(6,'new names service','new names service',0,500,8);
INSERT INTO `Services` VALUES(7,'group1 servie','sd',0,1300,6);
INSERT INTO `Services` VALUES(8,'New New test','This is the new new tet',0,500,6);
INSERT INTO `Services` VALUES(9,'tete','qq',0,200,6);
CREATE TABLE `ServiceGroup` (
	`servicegroupid`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT,
	`description`	TEXT,
	`state`	INTEGER
);
INSERT INTO `ServiceGroup` VALUES(6,'GR1','gr',0);
INSERT INTO `ServiceGroup` VALUES(7,'kanishkagroup','test',0);
INSERT INTO `ServiceGroup` VALUES(8,'new name','nir',0);
INSERT INTO `ServiceGroup` VALUES(9,'new datavalue','k',1);
CREATE TABLE "Role_Priviledge" (
	`rolename`	TEXT,
	`pricode`	INTEGER
);
INSERT INTO `Role_Priviledge` VALUES('Admin',1);
INSERT INTO `Role_Priviledge` VALUES('Admin',2);
CREATE TABLE `Role` (
	`rolename`	TEXT,
	`roledescription`	TEXT
);
INSERT INTO `Role` VALUES('General','General User');
INSERT INTO `Role` VALUES('Admin','Administrator user');
CREATE TABLE `Priviledge` (
	`pricode`	INTEGER,
	`name`	TEXT,
	`prevdescription`	TEXT
);
INSERT INTO `Priviledge` VALUES(2,'ManageUsers','Update Existing Users');
INSERT INTO `Priviledge` VALUES(1,'AddUsers','Add New Users');
CREATE TABLE "Payment" (
	`paymentid`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`orderid`	INTEGER,
	`amount`	NUMERIC,
	`reason`	TEXT,
	`takenby`	TEXT,
	`date`	TEXT
);
INSERT INTO `Payment` VALUES(2,9,4000,'','Admin','2016/02/22');
CREATE TABLE "Login" (
	`username`	TEXT,
	`password`	TEXT,
	`name`	TEXT,
	`rolename`	TEXT,
	PRIMARY KEY(username)
);
INSERT INTO `Login` VALUES('Admin',12,'Kanishka Weerasekara','Admin');
INSERT INTO `Login` VALUES('kan',123,'Kanishka','General');
INSERT INTO `Login` VALUES('gen',12,'gen','General');
CREATE TABLE "Customer" (
	`phone`	INTEGER,
	`name`	TEXT,
	`address`	TEXT,
	`email`	TEXT,
	`other`	TEXT,
	`dateofbirth`	TEXT,
	`nid`	TEXT,
	`registrationdate`	TEXT,
	`registedby`	TEXT
);
INSERT INTO `Customer` VALUES(718204549,'Kanishka Weerasekara','Diyathalawa','default@default.com','new user','1987/01/30','870851158v','2016/01/30','Admin');
INSERT INTO `Customer` VALUES(112560175,'John Keels','90/location. mars','default@default.com','-','2016/01/30','-','2016/01/30','Admin');
INSERT INTO `Customer` VALUES(728204547,'කණිෂ්ක','මාල ඹේ','default@default.com','-','2016/02/14','-','2016/02/14','Admin');
CREATE TABLE "ClientOrder" (
	`orderid`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`customerid`	INTEGER,
	`deliverydate`	TEXT,
	`asignee`	TEXT,
	`takenby`	TEXT,
	`comment`	TEXT,
	`state`	INTEGER
);
INSERT INTO `ClientOrder` VALUES(2,718204549,'2016/02/20','Admin','Admin','qaa',0);
INSERT INTO `ClientOrder` VALUES(8,728204547,'2016/02/21','Admin','Admin','tetstaaa',0);
INSERT INTO `ClientOrder` VALUES(9,718204549,'2016/02/21','Admin','Admin','N/A',0);
INSERT INTO `ClientOrder` VALUES(10,728204547,'2016/02/22','Admin','Admin','N/A',1);
;
COMMIT;
