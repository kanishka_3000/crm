/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "Task.h"

Task::Task()
{
    s_PersistatName = TBLNM_CRM_TASK;
    s_DefName = TBLNM_CRM_TASK;
    s_Key = FDNM_CRM_TASK_TASKID;
    p_Service  = nullptr;
    d_TotalValue = 0.0;
}

void Task::SetService(std::shared_ptr<Service> pService)
{
    p_Service = pService;
    map_Data.insert(FDNM_CRM_TASK_SERVICEID, p_Service->GetServiceID());
}

std::shared_ptr<Service> Task::GetService()
{
    return p_Service;
}



int Task::GetCount()
{
    return map_Data.value(FDNM_CRM_TASK_COUNT).toInt();
}

void Task::SetCount(int iCount)
{

    if(b_Update)
    {
        SetUpdateData(FDNM_CRM_TASK_COUNT, iCount);
    }
    AddData(FDNM_CRM_TASK_COUNT, iCount);
}

QString Task::GetDeliveryDate()
{
    return map_Data.value(FDNM_CRM_TASK_DELIVERYDATE).toString();
}

void Task::SetDeliveryDate(QString sDate)
{
    map_Data.insert(FDNM_CRM_TASK_DELIVERYDATE,sDate);
}

QString Task::GetAssignee()
{
    return map_Data.value(FDNM_CRM_TASK_ASSIGNEE).toString();
}

void Task::SetAssignee(QString sAssignee)
{
    map_Data.insert(FDNM_CRM_TASK_ASSIGNEE,sAssignee);
}

void Task::SetOrderID(int iOrderId)
{
    map_Data.insert(FDNM_CRM_TASK_ORDERID,iOrderId);
}

void Task::ResolveServices()
{
    if(p_Service != nullptr)
        return;

    QString sServiceID = GetData(FDNM_CRM_TASK_SERVICEID).toString();
    if(sServiceID.isEmpty())
        return;
    p_Service = std::static_pointer_cast<Service>(Entity::FindInstance(sServiceID,TBLNM_CRM_SERVICES));
}

void Task::SetTotalValue()
{
    d_TotalValue = (GetCount() * GetService()->GetValue());
}

QVariant Task::GetData(QString sFieldName)
{
    if(sFieldName == "price")
    {

        return  d_TotalValue;
    }

    return Entity::GetData(sFieldName);
}

