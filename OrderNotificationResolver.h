/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ORDERNOTIFICATIONRESOLVER_H
#define ORDERNOTIFICATIONRESOLVER_H
#include <NotificationCtrl.h>

class OrderNotificationResolver:public NotificationPopulator
{
public:
    OrderNotificationResolver();
     virtual void GetData(int iYear, int iMonth, QList<QDate>& lstDates, QList<QString>& lstNotifications)override;
    virtual void OnDateSelected(QDate& oDate) override;
};

#endif // ORDERNOTIFICATIONRESOLVER_H
