/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ADDSERVICEGROUPCTRL_H
#define ADDSERVICEGROUPCTRL_H

#include <QWidget>
#include "ui_AddServiceGroupCtrl.h"
#include <EntityManipulaterCtrl.h>
class AddServiceGroupCtrl : public EntityManipulaterCtrl, public Ui::AddServiceGroupCtrl
{
    Q_OBJECT

public:
    explicit AddServiceGroupCtrl(QWidget *parent = 0);
    ~AddServiceGroupCtrl();
    void OnCreateCtrl();
    virtual void PopulateWithEntity(std::shared_ptr<Entity> pEntity);
    virtual void OnPreSave();
public slots:
    void OnClose();
private:

};

#endif // ADDSERVICEGROUPCTRL_H
