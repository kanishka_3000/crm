/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef SERVICEDATAHANDLER_H
#define SERVICEDATAHANDLER_H

#include <GenericTableModel.h>

#define VALUE_FIELD  2
class ServiceDataHandler : public GenericTableDataCalback
{
public:
    ServiceDataHandler();
     virtual void GetRow(const WindowConfig* pWindowConfig, std::shared_ptr<void> pData, QVector<QVariant>& rData) override;
     virtual void OnDataChanged(const WindowConfig* pWindowConfig, int iColumn, std::shared_ptr<void> pData, QVariant oValue) override;
    virtual ~ServiceDataHandler(){}
};

#endif // SERVICEDATAHANDLER_H
