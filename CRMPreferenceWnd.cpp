/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "CRMPreferenceWnd.h"
#include <CMSPreferenceTab.h>
CRMPreferenceWnd::CRMPreferenceWnd(MainWnd *parent, QString sTitle):
    PrefereneWnd(parent, sTitle)
{

}

void CRMPreferenceWnd::AddCustomTabs()
{
    BasePreferenceTab* pCtrl = new CMSPreferenceTab(this);
    AddTab(pCtrl, tr("Content Management"));

}
