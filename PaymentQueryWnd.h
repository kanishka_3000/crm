/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef PAYMENTQUERYWND_H
#define PAYMENTQUERYWND_H

#include "ui_PaymentQueryWnd.h"
#include <GenericWnd.h>
class PaymentQueryWnd : public GenericWnd, private Ui::PaymentQueryWnd
{
    Q_OBJECT

public:
    explicit PaymentQueryWnd(MainWnd *parent,QString sTitle);

    // GenericWnd interface
public:
    virtual void OnCreate() override;
};

#endif // PAYMENTQUERYWND_H
