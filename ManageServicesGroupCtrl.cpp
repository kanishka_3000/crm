/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ManageServicesGroupCtrl.h"
#include <crmglobaldefs.h>

ManageServicesGroupCtrl::ManageServicesGroupCtrl(QWidget *pParent) :
    GenericCtrl(pParent)
{
    setupUi(this);

}

ManageServicesGroupCtrl::~ManageServicesGroupCtrl()
{

}



